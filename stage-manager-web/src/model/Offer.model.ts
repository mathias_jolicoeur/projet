export class Offer {
    private _id?: number;
    private _title?: string;
    private _description: string;
    private _jobType?: string;
    private _contact?: string;
    private _isValidated: boolean;
    //TODO un etudiant ou array?
    // etudiant?: IEtudiant;

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get title(): string {
        return this._title;
    }

    set title(value: string) {
        this._title = value;
    }

    get description(): string {
        return this._description;
    }

    set description(value: string) {
        this._description = value;
    }

    get jobType(): string {
        return this._jobType;
    }

    set jobType(value: string) {
        this._jobType = value;
    }

    get contact(): string {
        return this._contact;
    }

    set contact(value: string) {
        this._contact = value;
    }
    get isValidated(): boolean {
        return this.isValidated;
    }

    set isValidated(value: boolean) {
        this.isValidated = value;
    }
}
