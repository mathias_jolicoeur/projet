import React from 'react';

import Upload from './upload';

class UploadCV extends React.Component {
    render() {
        return (
            <div className="UploadCV">
                <Upload state={[]} />
            </div>
        );
    }
}

export default UploadCV;
