import React, { Component } from 'react';
import './App.css';
import {BrowserRouter as Router, Route} from 'react-router-dom'
import Upload from './components/Upload/upload.js'
import Login from './components/Authentication/login.js'
import CreerOffreStage from './components/internship-offers/creerOffreStage.js'
import ModifieOffreStage from './components/internship-offers/modifieOffreStage.js'
import Menu from './components/menu/menu.js'
import InternshipOffers from './components/internship-offers/internship-offers.js'
import InternshipOfferDetail from './components/internship-offers/internship-offer-detail.js'
import Register from './components/Authentication/register.js'
import Home from './components/Home/home.js'
import ShowUsers from './components/User/showUsers.js'
import UserDetail from './components/User/user-detail.js'
import User from './components/User/user.js'
import ValideApplicationEtudiant from './components/applicationStage/valideApplicationEtudiant.js'
import ApplicationStage from './components/applicationStage/applicationStage.js'
import Notification from './components/Notifications/notifications.js'
import UpdateStatus from './components/updateStatus/updateStatus.js'
import Forms from './components/forms/forms.js'
import AccepteStage from './components/applicationStage/accepteStage.js'
import StageEtudiant from './components/Stage/stageEtudiant.js'
import Footer from './components/Footer/footer.js'


class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <header className="App-header">
              <h1> STAGE MANAGER </h1>
              <Menu />
          </header>

          <Route path="/home" component={Home} />
          <Route path="/upload" component={Upload} />
          <Route path="/Authentication/login" component={Login} />
          <Route path="/creeroffrestage" component={CreerOffreStage} />
          <Route path="/modifieoffrestage/:offresId" component={ModifieOffreStage} />
          <Route path="/Authentication/register" component={Register} />
          <Route path="/internshipOffers" component={InternshipOffers} />
          <Route path="/internshipOfferDetail/:offresId" component={InternshipOfferDetail} />
          <Route path="/showUsers" component={ShowUsers} />
          <Route path="/updateStatus" component={UpdateStatus} />
          <Route path="/userDetail/:userId" component={UserDetail} />
          <Route path="/user" component={User} />
          <Route path="/valideApplicationEtudiant" component={ValideApplicationEtudiant} />
          <Route path="/applicationStage" component={ApplicationStage} />
          <Route path="/notifications" component={Notification} />
          <Route path="/formulaires" component={Forms} />
          <Route path="/accepteStage" component={AccepteStage} />
          <Route path="/stageEtudiant" component={StageEtudiant} />

          <footer className="App-footer">
            <Footer />
          </footer>
        </div>
      </Router>
    );
  }
}

export default App;
