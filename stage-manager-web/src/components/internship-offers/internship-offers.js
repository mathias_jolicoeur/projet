import './internship-offers.css';
import React, {Component} from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import _ from 'lodash';
import { Button, Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import 'react-table/react-table.css';
import ReactTable from 'react-table'


export default class InternshipOffers extends Component {
    constructor(props){
        super(props);
        var userInfo = JSON.parse(localStorage.getItem("account"));
        if(!userInfo){
          window.location = `/Authentication/login`;
        }
        this.state = {
          PATH: "http://localhost:8080/",
          currUser: userInfo,
          currUserType: userInfo.type.toLowerCase(),
          intershipOffers: [],
          intershipOffersFiltered: [],
          isValidated: true,
          validatedBtnLabel: "Offres a valider",
          page: 0,
          pageSize: 10,
          pages: null,
          dropdownOpen: false,
          session: "automne 2018",
          isCompany: userInfo.type.toLowerCase()===`compagnie`
        };
        this.getIsValidatedAndSession = this.getIsValidatedAndSession.bind(this);
        this.toggle = this.toggle.bind(this);
        this.handleSessionChange = this.handleSessionChange.bind(this);
    }
    componentDidMount() {
      switch(this.state.currUserType){
        case "etudiant":
           this.getIsValidatedAndSession(true, this.state.session);
          break;
        case "coordonateur":
           this.getIsValidatedAndSession(true, this.state.session);
          break;
        case "compagnie":
          this.handleSessionChange(this.state.session, true, this.state.currUser.id);
          break;
      }
    }
    getIsValidatedAndSession(isValidated, session){
      this.setState({session: session});
      axios.get(this.state.PATH + 'offreStage/validatedAndSession?isValidated=' + new Boolean(isValidated).toString() +
        '&session=' + session + '&page=' + this.state.page + '&size=' + this.state.pageSize).then(res => {
        const offers = res.data.content;
        this.setState({ intershipOffers: offers,
                        pages: res.data.totalPages,
                        loading: false});
      });
    }
    handleTypeData(session, isValidated, createdById){
      if(this.state.isCompany){
        this.handleSessionChange(session, isValidated, createdById);
      } else {
        this.getIsValidatedAndSession(isValidated, session);
      }
    }
    handleSessionChange(session, isValidated, createdById) {
      this.setState({session: session}, function() {
        axios.get(this.state.PATH + 'offreStage/session?sessionName=' + this.state.session +
        '&createdById=' + new String(createdById) + '&isValidated=' + new Boolean(isValidated).toString() +
        '&page=' + this.state.page + '&size=' + this.state.pageSize).then(res => {
        const offers = res.data.content;
        this.setState({ intershipOffers: offers,
                        //isValidated:true,
                        validatedBtnLabel:this.state.isValidated?"Offres à valider":"Offres validé",
                        pages: res.data.totalPages,
                        loading: false});
        });
      })
    }
    toggle(event) {
      this.setState({
        dropdownOpen: !this.state.dropdownOpen
      })
    }
    handleChangeIsValidated(){
      this.setState({
        isValidated: this.state.isValidated?false:true,
        validatedBtnLabel: this.state.isValidated?"Offres a valider":"Offres validé"
      }, function() {
          this.getIsValidatedAndSession(this.state.isValidated, this.state.session);
      });
    }
    render() {
      var columns = [
        {
          Header : () => <strong>Offre </strong>,
          accessor : 'id',
          Cell: ({value}) => (<Link to={`/internshipOfferDetail/${value}`}>{value}</Link>)
        },{
          Header : () => <strong>Nom de la compagnie</strong>,
          accessor : 'nomCompagnie'
        },
        {
          Header : () => <strong>Poste</strong>,
          accessor : 'posteARemplir'
        }
      ]
        return (
            <div className="container">
                <h3>Liste d'offres</h3>
                <p>Voici les offres de stage disponible</p>
                <Dropdown className="dropdownStatus" isOpen={this.state.dropdownOpen} toggle={this.toggle}>
                  <DropdownToggle caret>
                    {this.state.session}
                  </DropdownToggle>
                  <DropdownMenu>
                    <DropdownItem key={"aut18"} onClick={(event)=>this.handleTypeData( event.currentTarget.getAttribute("dropdownvalue"), this.state.isValidated, this.state.currUser.id)} dropdownvalue={"automne 2018"}>{"automne 2018"}</DropdownItem>
                    <DropdownItem key={"hiv19"} onClick={(event)=>this.handleTypeData( event.currentTarget.getAttribute("dropdownvalue"), this.state.isValidated, this.state.currUser.id)} dropdownvalue={"hiver 2019"}>{"hiver 2019"}</DropdownItem>
                    <DropdownItem key={"ete19"} onClick={(event)=>this.handleTypeData( event.currentTarget.getAttribute("dropdownvalue"), this.state.isValidated, this.state.currUser.id)} dropdownvalue={"ete 2019"}>{"ete 2019"}</DropdownItem>
                  </DropdownMenu>
                </Dropdown>
                {this.state.currUserType !== 'etudiant' ? <div><Link to={`../creerOffreStage`}><Button color="secondary">Créer une offre</Button></Link>
                 <Button color="secondary" onClick={()=>this.handleChangeIsValidated()}>
                  {this.state.validatedBtnLabel}
                  </Button></div>
                  :""}
                  <br/>
                <h3 style={{textDecoration:'underline'}}>{this.state.isValidated?"Offres validées":"Offres à valider"}</h3>
                <ReactTable
                  columns = {columns}
                  manual
                  data={this.state.intershipOffers}
                  pages={this.state.pages}
                  defaultPageSize={10}
                  loading={this.state.loading}
                  onFetchData={()=>this.handleTypeData(null, this.state.isValidated,null)}
                />
            </div>
        );
    }
}
