import React, { Component } from 'react';
import axios from 'axios';
import { Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import ValideApplicationEtudiant from '../applicationStage/valideApplicationEtudiant.js'

export default class InternshipOfferDetail extends Component {

    constructor(props) {
        super(props);
        var userInfo = JSON.parse(localStorage.getItem("account"));
        this.state = {
          PATH: "http://localhost:8080/offreStage/",
          id: this.props.match.params.offresId,
          nomCompagnie: '',
          posteARemplir: '',
          salaire: '',
          description: '',
          isValidated: false,
          session: "automne 2018",
          currUser: userInfo,
          showApplication: false,
          currUserType: userInfo.type.toLowerCase()
        };
        if(!this.state.currUser){
          window.location = "/Authentication/login";
        }
        this.renderCoordonatorOptions = this.renderCoordonatorOptions.bind(this);
        this.renderOptionsByUserType = this.renderOptionsByUserType.bind(this);
        this.renderCompanyOptions = this.renderCompanyOptions.bind(this);
      }

  componentDidMount() {
    this.getOffersById(this.props.match.params.offresId);
    if(this.state.currUserType === "coordonateur"){
      this.setState({showApplication: true});
    }
  }
  getOffersById(id){
    axios.get(`${this.state.PATH}${id}`).then(res => {
      const offre = res.data;
      this.setState({nomCompagnie: offre.nomCompagnie,
                     posteARemplir: offre.posteARemplir,
                     salaire: offre.salaire,
                     description: offre.description,
                     isValidated: offre.validated,
                     session: offre.session
                    });

    })
  }
   handleSubmit = event => {
    event.preventDefault();
    this.validOffer(this.props.match.params.offresId);
  }
  validOffer(id){
    axios.post(`${this.state.PATH}update/${id}`, {
      id: this.props.match.params.offresId,
      nomCompagnie: this.state.nomCompagnie,
      posteARemplir: this.state.posteARemplir,
      salaire: this.state.salaire,
      description: this.state.description,
      isValidated: true,
      session: this.state.session
    }).then(res => {
      this.setState({isValidated: true})
    }).catch(error => {
    });
  }
  handleRefuse = event => {
    event.preventDefault();
  }
  handleApplication = event => {
    event.preventDefault();
    this.validApplication();
  }
  validApplication(){
    axios.post(`${this.state.PATH}addUserId/${this.state.currUser.id}/${this.state.id}`, {
    }).then(res => {
      alert("Vous avez appliqué!");
    }).catch(error => {
    });
  }
  renderStudentOptions(){
    if(this.state.isValidated){
      return (<Button color="primary" onClick={this.handleApplication}>Appliquer</Button>);
    }
    return "";
  }
  renderCoordonatorOptions(){
    if(!this.state.isValidated){
      return (<div>
                  <Button color="success" onClick={this.handleSubmit}>Valider</Button>
              </div>);
    } else {
      return (<div>
                  <Button color="danger" onClick={this.handleRefuse}>Refuser</Button>
              </div>);
    }
  }
  renderCompanyOptions(){
    if(this.state.isValidated){
      return (<Link to={`/modifieoffreStage/${this.state.id}`}><Button color="danger">Modifier</Button></Link>);
    }
    return "";
  }
  renderOptionsByUserType(){
    var render;
    switch(this.state.currUserType){
      case "etudiant":
        render = this.renderStudentOptions();
        break;
      case "coordonateur":
        render = this.renderCoordonatorOptions();
        break;
      case "compagnie":
          render = this.renderCompanyOptions();
          break;
    }
    return render;
  }
  render() {
    return (
      <div className="ModifieOffreStage w-50 p-3 mx-auto">
        <h3>Offre #{this.state.id}</h3>
        <p>Nom de la compagnie: {this.state.nomCompagnie}</p>
        <p>Poste à remplir: {this.state.posteARemplir}</p>
        <p>Salaire par heure: {this.state.salaire}</p>
        <p>Description: {this.state.description}</p>
        <p>Session: {this.state.session}</p>
        {this.renderOptionsByUserType()}
        <Link to={`../internshipOffers`}><Button color="secondary">Retour</Button></Link>
        {this.state.showApplication ?
          <ValideApplicationEtudiant offerId={this.state.id} /> : ""}
      </div>
    );
  }
}
