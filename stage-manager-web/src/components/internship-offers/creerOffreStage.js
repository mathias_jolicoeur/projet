import React, { Component } from 'react';
import { Button, Form, FormGroup, Input, Dropdown, DropdownItem, DropdownMenu, DropdownToggle} from 'reactstrap';
import axios from 'axios';


class CreerOffreStage extends Component {
  constructor(props){
    super(props);
    var userInfo = JSON.parse(localStorage.getItem("account"));
    this.state = {
      currUser: userInfo,
      nomCompagnie: '',
      posteARemplir: '',
      salaire: '',
      description: '',
      dropdownOpen: false,
      session: "automne 2018"
    };
    if(!this.state.currUser){
      window.location = "/Authentication/login";
    }
    this.toggle = this.toggle.bind(this);
    this.handleSessionChange = this.handleSessionChange.bind(this);
  }


  handleNomCompagnieChange = event => {this.setState({ nomCompagnie: event.target.value })}
  handlePosteARemplirChange = event => {this.setState({ posteARemplir: event.target.value })}
  handleSalaireChange = event => {this.setState({ salaire: event.target.value })}
  handleDescriptionChange = event => {this.setState({ description: event.target.value })}
  handleSessionChange = event => {this.setState({session: event.currentTarget.getAttribute("dropdownvalue")})}

  toggle(event) {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    })
  }

  handleSubmit = event => {
    event.preventDefault();
    axios.post('http://localhost:8080/offreStage/createOffreStage', {
      nomCompagnie: this.state.nomCompagnie,
      posteARemplir: this.state.posteARemplir,
      salaire: this.state.salaire,
      description: this.state.description,
      isValidated: false,
      session: this.state.session,
      createdById: this.state.currUser.id
    }).then(res => {
      window.location = "/internshipOffers";
    });
  }
  render() {
    return (
      <div className="CreerOffreStage w-50 p-3 mx-auto">
        <h3>Créer un offre de stage</h3>
        <Form onSubmit={this.handleSubmit}>
          <FormGroup>
            <Input onChange={this.handleNomCompagnieChange} type="text" name="nomCompagnie" id="nomCompagnie" placeholder="Nom de la compagnie" required />
          </FormGroup>
          <FormGroup>
            <Input onChange={this.handlePosteARemplirChange} type="text" name="posteARemplir" id="posteARemplir" placeholder="Poste à remplir" required />
          </FormGroup>
          <FormGroup>
            <Input onChange={this.handleSalaireChange} type="text" name="salaire" id="salaire" placeholder="Salaire par heure" required />
          </FormGroup>
          <FormGroup>
            <Input onChange={this.handleDescriptionChange} type="textarea" name="description" id="descrition" placeholder="Description des tâches" required />
          </FormGroup>
          <Dropdown className="dropdownStatus" isOpen={this.state.dropdownOpen} toggle={this.toggle}>
            <DropdownToggle caret>
              {this.state.session}
            </DropdownToggle>
            <DropdownMenu>
              <DropdownItem key={"aut18"} onClick={this.handleSessionChange} dropdownvalue={"automne 2018"}>{"automne 2018"}</DropdownItem>
              <DropdownItem key={"hiv19"} onClick={this.handleSessionChange} dropdownvalue={"hiver 2019"}>{"hiver 2019"}</DropdownItem>
              <DropdownItem key={"ete19"} onClick={this.handleSessionChange} dropdownvalue={"ete 2019"}>{"ete 2019"}</DropdownItem>
            </DropdownMenu>
          </Dropdown>
          <FormGroup className="btn-toolbar">
            <Button className="danger" type="submit">{"Créer l'offre"}</Button>
          </FormGroup>
        </Form>
      </div>
    );
  }
}
export default CreerOffreStage;
