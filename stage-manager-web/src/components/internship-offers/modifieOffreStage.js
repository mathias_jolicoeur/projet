import React, { Component } from 'react';
import { Button, Form, FormGroup, Input } from 'reactstrap';
import { Link } from 'react-router-dom';
import axios from 'axios';


class ModifieOffreStage extends Component {

    constructor(props) {
        super(props);
        var userInfo = JSON.parse(localStorage.getItem("account"));

        this.state = {
          PATH:"http://localhost:8080/offreStage/",
          currUser: userInfo,
          id: this.props.match.params.offresId,
          nomCompagnie: '',
          posteARemplir: '',
          salaire: '',
          description: '',
          session: ''
        };

        if(!this.state.currUser){
          window.location = "/Authentication/login";
        }
      }



  componentDidMount() {
    axios.get(`${this.state.PATH}${this.props.match.params.offresId}`).then(res => {
      const offres = res.data;
      this.setState({nomCompagnie: offres.nomCompagnie,
                     posteARemplir: offres.posteARemplir,
                     salaire: offres.salaire,
                     description: offres.description,
                     session:offres.session
                    });
    })

  }

  handleNomCompagnieChange = event => {this.setState({ nomCompagnie: event.target.value })}
  handlePosteARemplirChange = event => {this.setState({ posteARemplir: event.target.value })}
  handleSalaireChange = event => {this.setState({ salaire: event.target.value })}
  handleDescriptionChange = event => {this.setState({ description: event.target.value })}

  handleSubmit = event => {
    event.preventDefault();

    axios.post(this.state.PATH + 'createOffreStage', {
      id: this.state.id,
      nomCompagnie: this.state.nomCompagnie,
      posteARemplir: this.state.posteARemplir,
      salaire: this.state.salaire,
      description: this.state.description,
      session: this.state.session
    }).then(res=>{
      window.alert("Modification effectuee");
      window.location = "/internshipOfferDetail/" + this.state.id ;
    })
  }
  render() {
    return (
      <div className="ModifieOffreStage w-50 p-3 mx-auto">
        <h3>Modifier l'offre de stage</h3>

        <Form onSubmit={this.handleSubmit}>
           <FormGroup>
             <Input onChange={this.handleNomCompagnieChange} type="text" name="nomCompagnie" id="nomCompagnie" placeholder="Nom de la compagnie" value={this.state.nomCompagnie} required />
           </FormGroup>
           <FormGroup>
             <Input onChange={this.handlePosteARemplirChange} type="text" name="posteARemplir" id="posteARemplir" placeholder="Poste à remplir" value={this.state.posteARemplir} required />
           </FormGroup>
           <FormGroup>
             <Input onChange={this.handleSalaireChange} type="text" name="salaire" id="salaire" placeholder="Salaire par heure"  value={this.state.salaire}required />
           </FormGroup>
           <FormGroup>
             <Input onChange={this.handleDescriptionChange} type="textarea" name="description" id="descrition" placeholder="Description des tâches" value={this.state.description} required />
           </FormGroup>
           <FormGroup className="btn-toolbar">
              <Button className="danger" type="submit">Modifier</Button>
                <Link to={`../internshipOfferDetail/${this.state.id}`}><Button color="secondary">Retour</Button></Link>
            </FormGroup>
        </Form>
      </div>
    );
  }
}
export default ModifieOffreStage;
