import './upload.css';
import React, {Component} from 'react';
import {Input, FormGroup, Button} from 'reactstrap';
import axios from 'axios'

class Upload extends Component {
  constructor(props) {
    super(props);
    var userInfo = JSON.parse(localStorage.getItem("account"));
    this.state = {
      fileToSend:  null,
      currUser: userInfo
    }
  }

  onChange = event => {
    this.setState({
      fileToSend: event.target.files[0]
    })
  }

  onClick = () => {
    const formdata = new FormData();
    formdata.append('file', this.state.fileToSend);
    axios.post('http://localhost:8080/user/createCv?userId=%27'+this.state.currUser.id, formdata)
    .then(res => {
      window.alert("Soumission du CV réussi!");
    });
  }

  render() {
    return (
      <div className="Login w-50 p-3 mx-auto">
        <h1>{"Soumettez votre CV"}</h1>
          <FormGroup>
            <Input type="file" name="fileToSend" accept=".pdf" multiple={false} onChange={this.onChange} required className = "fileInput"></Input>
          </FormGroup>
          <FormGroup>
            <Button onClick={this.onClick}>{"Soumettre"}</Button>
          </FormGroup>
      </div>
    );
  }
}

export default Upload;