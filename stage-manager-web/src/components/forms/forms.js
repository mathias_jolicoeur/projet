import React, { Component } from 'react';
import axios from 'axios';
import {Input, FormGroup, Button} from 'reactstrap';

export default class Forms extends Component {

    constructor(props) {
        super(props);
        var userInfo = JSON.parse(localStorage.getItem("account"));
        this.state = {
          PATH: "http://localhost:8080/",
          currUser: userInfo,
          showApplication: false,
          currUserType: userInfo?userInfo.type.toLowerCase():null,
          newEvaluationForm: null,
          newHourWorkedForm: null
        };
        if(!this.state.currUser){
          window.location = "/Authentication/login";
        }
        this.onChangeEvaluationForm = this.onChangeEvaluationForm.bind(this);
        this.handleSubmitFormEvaluation = this.handleSubmitFormEvaluation.bind(this);
        this.handleDownloadEvaluation = this.handleDownloadEvaluation.bind(this);
      }
  onChangeEvaluationForm = event => {
    this.setState({newEvaluationForm:event.target.files[0]});
  }
  onChangeHourWorkedForm = event => {
    this.setState({newHourWorkedForm:event.target.files[0]});
  }
  handleSubmitFormEvaluation = event => {
    var formdata = new FormData();
    formdata.append('file', this.state.newEvaluationForm);
    axios.post(this.state.PATH + 'user/formEvaluation?userId='+this.state.currUser.id, formdata)
    .then(res => {
      this.setState({
        newEvaluationForm:null
      });
      window.alert("Le formulaire d'évaluation du stagiaire à été changé!");
    });
  }
  handleSubmitFormHours = event => {
    const formdata = new FormData();
    formdata.append('file', this.state.newHourWorkedForm);
    axios.post(this.state.PATH + 'user/formHours?userId='+this.state.currUser.id, formdata)
    .then(res => {
      this.setState({
        newHourWorkedForm:null
      });
      window.alert("Le formulaire des heures travaillées à été changé!");
    });
  }
  handleDownloadEvaluation = event => {
    axios.get(this.state.PATH + '/user/downloadEvaluation?id=' + this.state.currUser.id)
      .then(res => {
        var byteArray = new Uint8Array(res);
        var blob = new Blob([byteArray], {type: 'application/pdf'});
        let url = window.URL.createObjectURL(blob);
        let a = document.createElement('a');
        a.href = url;
        a.download = "Evaluation.pdf";
        a.click();
      })
  }
  handleDownloadHours = event => {
    axios.get(this.state.PATH + '/user/downloadHours?id=' + this.state.currUser.id)
      .then(res => {
        var byteArray = new Uint8Array(res);
        var blob = new Blob([res], {type: 'application/pdf'});
        let url = window.URL.createObjectURL(blob);
        let a = document.createElement('a');
        a.href = url;
        a.download = "Heures.pdf";
        a.click();
      })
  }

  render() {
    return (
      <div className="ModifieOffreStage w-50 p-3 mx-auto">
        <h3>Formulaires</h3>
        <p>Voici les formulaires que vous devez t&eacute;l&eacute;charger et remplir pour completer
          le stage de votre stagiaire</p>

          <p><Button onClick={this.handleDownloadEvaluation}>&Eacute;valuation du stagiare</Button></p>
          {this.state.currUserType === 'coordonateur'?<div>
          <FormGroup>
            <Input type="file" name="fileToSend" accept=".pdf" multiple={false} onChange={this.onChangeEvaluationForm} required className = "fileInput"></Input>
          </FormGroup>
          <FormGroup>
            <Button onClick={this.handleSubmitFormEvaluation}>{"Soumettre"}</Button>
          </FormGroup></div> : ""
          }
          <p><Button onClick={this.handleDownloadHours}>Heures travaill&eacute;es</Button></p>
          {this.state.currUserType === 'coordonateur'?<div>
          <FormGroup>
            <Input type="file" name="fileToSend" accept=".pdf" multiple={false} onChange={this.onChangeHourWorkedForm} required className = "fileInput"></Input>
          </FormGroup>
          <FormGroup>
            <Button onClick={this.handleSubmitFormHours}>{"Soumettre"}</Button>
          </FormGroup></div> : ""
          }
      </div>
    );
  }
}
