import React, { Component } from 'react';
import axios from 'axios';
import { Button, Form, FormGroup, Input } from 'reactstrap';
import Slogan from '../Slogan/slogan';

class Register extends Component {
  state = {
    email: '',
    password: '',
    confirmPwd:'',
    isValid:'',
    type:'default',
    status: ''
  };

  handleEmailChange = event => {this.setState({ email: event.target.value })}
  handlePasswordChange = event => {this.setState({ password: event.target.value })}
  handleconfirmPwd = event => {this.setState({ confirmPwd:event.target.value })}
  handleSelectChange = event => {this.setState({ type:event.target.value })}

  handleSubmit = event => {
    event.preventDefault();
    if(this.state.type == "default"){
      alert('Veuillez choisir une option');
    }
    else if(this.state.password == this.state.confirmPwd) {
      axios.post('http://localhost:8080/user/createUser', {
        email: this.state.email,
        password: this.state.password,
        status: "Pas de stage",
        type: this.state.type
      }).then(res => {
        window.location = "/Authentication/login";
      });
    }else {
      alert('Les mots de passe ne sont pas pareil');
    }
  }

  render() {
    return (
      <div className="Register w-50 p-3 mx-auto">
      <Slogan />
      <img src={ require('../../images/bertrand.png') } />
        <Form onSubmit={this.handleSubmit}>
           <FormGroup>
             <Input onChange={this.handleEmailChange} type="email" name="email" id="email" placeholder="Entrez votre courriel" autoFocus/>
           </FormGroup>
           <FormGroup>
             <Input onChange={this.handlePasswordChange} type="password" name="password" id="password" placeholder="Entrez votre mot de passe" />
           </FormGroup>
           <FormGroup>
             <Input onChange={this.handleconfirmPwd} type="password" name="confirmPwd" id="confirmPwd" placeholder="Entrez une seconde fois votre mot de passe" />
           </FormGroup>
           <FormGroup>
              <Input type="select" value={this.state.value} onChange={this.handleSelectChange} id="selectRole">
                 <option value="default">Quel type d'utilisateur êtes-vous ?</option>
                 <option value="Etudiant">Étudiant</option>
                 <option value="Compagnie">Compagnie</option>
                 <option value="Moniteur">Moniteur</option>
               </Input>
           </FormGroup>
           <FormGroup className="btn-toolbar">
             <Button className="danger" type="submit">{"S'inscrire"}</Button>
           </FormGroup>
         </Form>
      </div>
    );
  }
}

export default Register;
