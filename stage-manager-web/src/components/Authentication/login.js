import React, { Component } from 'react';
import { Button, Form, FormGroup, Input } from 'reactstrap';
import axios from 'axios';
import Slogan from '../Slogan/slogan';

class Login extends Component {

  state = {
    email: '',
    password: ''
  };

  handleEmailChange = event => {this.setState({ email: event.target.value })}
  handlePasswordChange = event => {this.setState({ password: event.target.value })}


  handleSubmit = event => {
    event.preventDefault();

    axios.post('http://localhost:8080/user/login', {
      email: this.state.email,
      password: this.state.password
    }).then(res => {
      const user = res.data;
      if(user !== ''){
        localStorage.setItem("account", JSON.stringify(user));
        window.location = "/home";
      }

    });
  }
  render() {
    return (
      <div className="w-50 p-3 mx-auto">
        <Slogan />
        <img src={ require('../../images/bertrand.png') } />
        <Form onSubmit={this.handleSubmit}>
          <FormGroup>
            <Input onChange={this.handleEmailChange} type="email" name="email" id="email" placeholder="Entrez votre courriel" required  autoFocus/>
          </FormGroup>
          <FormGroup>
            <Input onChange={this.handlePasswordChange} type="password" name="password" id="password" placeholder="Entrez votre mot de passe" required />
          </FormGroup>
          <FormGroup className="btn-toolbar">
           <Button className="danger" type="submit">Se connecter</Button>
          </FormGroup>
        </Form>
      </div>
    );
  }
}

export default Login;
