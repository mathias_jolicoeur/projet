import React, { Component } from 'react';
import ReactTable from 'react-table'
import axios from 'axios';
import 'react-table/react-table.css';
import { Button, FormGroup, Input } from 'reactstrap';
import "./notification.css";

class Notification extends Component {
  constructor() {
    super();
    var userInfo = JSON.parse(localStorage.getItem("account"));
    this.state = {
      users: [],
      page: 0,
      pageSize: 10,
      pages: null,
      currUser: userInfo,
      currUserType: userInfo.type.toLowerCase(),
      userId: userInfo.id,
      columns: [],
      notifications:[],
      session: "",
      value:"",
      loading: true,
      requestToDo: this.handleNotificationsList
    };
    this.handleNotificationsList = this.handleNotificationsList.bind(this);
    this.findBySessionAndUserId = this.findBySessionAndUserId.bind(this);
  }

  async findBySessionAndUserId(event){
    this.setState({ session:event.target.value, loading: true });
    axios.get('http://localhost:8080/notification/notificationsSession?session=' + this.state.session +
      '&userId=' + this.state.userId + '&page=' + this.state.page + '&size=' + this.state.pageSize).then(res => {
      const notifications = res.data.content;
      this.setState({
        data:notifications,
        notifications: notifications,
        pages: res.data.totalPages,
        loading: false});
    });
  }

  async handleNotificationsList(state, instance) {
    this.setState({ loading: true });
    axios.get('http://localhost:8080/notification/page/notifications?page=' + state.page + '&size=' + state.pageSize)
      .then(res => {
        this.setState({
          data: res.data.content,
          pages: res.data.totalPages,
          loading: false
        });
      });
  }

  async handleDeleteNotification(id){
    this.setState({ loading: true });
    axios.delete('http://localhost:8080/notification/deleteNotification?id=' + id);
    this.setState({ loading: false });
    window.location = "/notifications";
  }

  render(){
    if(this.state.currUserType == "compagnie"){
      this.state.columns = [
        {
          Header : () => <strong>Arrivé du stagiaire:</strong>,
          accessor : 'nomStagiaire'
        },
        {
          Header : () => <strong>À partir du:</strong>,
          accessor : 'date'
        },
        {
          Header : () => <strong>Action</strong>,
          accessor : 'id',
          Cell: ({value}) => (<Button color="danger" onClick={()=>{this.handleDeleteNotification(value)}}>Supprimer</Button>)
        }
      ]
    }else{
      this.state.columns = [
        {
          Header : () => <strong>Offre de stage:</strong>,
          accessor : 'compagnie'
        },
        {
          Header : () => <strong>Poste à remplir:</strong>,
          accessor : 'posteARemplir'
        },
        {
          Header : () => <strong>Message:</strong>,
          Cell: ({}) => ("L'offre de stage a été complétée.")
        },
        {
          Header : () => <strong>Action</strong>,
          accessor : 'id',
          Cell: ({value}) => (<Button color="danger" onClick={()=>{this.handleDeleteNotification(value)}}>Supprimer</Button>)
        }
      ]

    }

    const { data, pages, loading } = this.state;

    return (
      <div className="notification">
        <h3>Mes notifications</h3>
        <FormGroup>
          <Input type="select" name="select" id="selectSession" value={this.state.value} onChange={this.findBySessionAndUserId}>
            <option value="A2018">Choisir une session</option>
            <option value="A2018">A2018</option>
            <option value="H2018">H2018</option>
            <option value="A2017">A2017</option>
            <option value="H2017">H2017</option>
          </Input>
        </FormGroup>
        <ReactTable
          columns = {this.state.columns}
          manual
          data={data}
          pages={pages}
          defaultPageSize={10}
          loading={loading}
          onFetchData={this.handleNotificationsList}
        />
      </div>
    )
  }
}

export default Notification;
