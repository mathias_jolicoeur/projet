import "./updateStatus.css";
import "react-widgets/dist/css/react-widgets.css";
import React, {Component} from 'react';
import axios from 'axios';
import _ from 'lodash';
import { Button, Form, FormGroup, Dropdown, DropdownItem, DropdownMenu, DropdownToggle, Label } from 'reactstrap';
import DateTimePicker from 'react-widgets/lib/DateTimePicker';
import momentLocalizer from 'react-widgets-moment';
import Moment from 'moment';

Moment.locale("en");
momentLocalizer();

export default class UpdateStatus extends Component {
    constructor(props){
        super(props);
        var userInfo = JSON.parse(localStorage.getItem("account"));
        this.state = {
          PATH: "http://localhost:8080/",
          currUser: userInfo,
          dropdownOpen: false,
          listStatus: ["Pas de stage", "Cédulé pour une entrevue", "stage trouvé" ],
          currStatus: userInfo.status,
          currDate: null,
          isInterviewStatus:userInfo.status.toLowerCase() == "cédulé pour une entrevue"
        };
        if(!this.state.currUser){
          window.location = "/Authentication/login";
        }
        this.toggle = this.toggle.bind(this);
        this.handleStatusChange = this.handleStatusChange.bind(this);
        this.updateInterviewDate = this.updateInterviewDate.bind(this);
    }
    toggle(event) {
      this.setState({
        dropdownOpen: !this.state.dropdownOpen,
      });
    }
    handleStatusChange(event){
      var status = event.currentTarget.getAttribute("dropdownvalue");
      this.setState({currStatus: status,
      isInterviewStatus:status.toLowerCase() == "cédulé pour une entrevue"});
    }
    handleSubmit = event => {
      event.preventDefault();
      var currUser = this.state.currUser;
      currUser.status = this.state.currStatus;
      if(!this.state.isInterviewStatus && this.state.currDate !== null){
        this.currDate = null;
      }
      this.setState({currUser: currUser});
      this.updateUser(this.state.currUser);
    }
    updateUser(user){
      axios.post('http://localhost:8080/user/update/' + user.id, {
        id: user.id,
        email: user.email,
        password: user.password,
        status: user.status,
        isValidated: user.isValidated,
        type: user.type,
        interviewDate: user.interviewDate,
        isActif: user.isActif
      }).then(res => {
        localStorage.setItem("account", JSON.stringify(user));
        window.location = "/updateStatus";
      });
    }
    updateInterviewDate(){
      var currUser = this.state.currUser;
      currUser.interviewDate = this.state.currDate;
      this.updateUser(currUser);
    }

    render() {
        return (
            <div className="container">
                <h3>Modifier le statut</h3>
                <Form onSubmit={this.handleSubmit}>
                <Label className="statusLabel">Status: </Label>
                  <Dropdown className="dropdownStatus" isOpen={this.state.dropdownOpen} toggle={this.toggle}>
                    <DropdownToggle caret>
                      {this.state.currStatus}
                    </DropdownToggle>
                   <DropdownMenu>
                     {this.state.listStatus.map((status, index) => (
                       <DropdownItem key={index} onClick={this.handleStatusChange} dropdownvalue={status}>{status}</DropdownItem>
                    ))}
                   </DropdownMenu>
                   </Dropdown>
                   {this.state.isInterviewStatus?<DateTimePicker onChange={value=>this.setState({currDate: value})} defaultValue={new Date()}/>:""}
                   <br/>
                   <FormGroup className="btn-toolbar">
                   {this.state.isInterviewStatus?<Button className="secondary" onClick={this.updateInterviewDate}>Céduler</Button>:""}
                     <Button className="secondary" type="submit">Mettre &agrave; jour le statut</Button>
                   </FormGroup>
                </Form>
            </div>
        );
    }
}
