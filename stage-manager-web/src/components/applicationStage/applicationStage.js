import React, { Component } from 'react';
import ReactTable from 'react-table'
import axios from 'axios';
import {Link} from 'react-router-dom';
import 'react-table/react-table.css';

class ApplicationStage extends Component {
  constructor() {
    super();
    this.state = {
      applications: [],
      page: 0,
      pageSize: 10,
      pages: null
    };
    this.handleApplicationList = this.handleApplicationList.bind(this);
  }

  async handleApplicationList(state, instance) {
    this.setState({ loading: true });
    axios.get('http://localhost:8080/application/page/applications?page=' + state.page + '&size=' + state.pageSize)
      .then(res => {
        this.setState({
          data: res.data.content,
          pages: res.data.totalPages,
          loading: false
        });
      })
  }

  render(){
    const columns = [
      {
        Header : () => <strong>Email</strong>,
        accessor : 'user.email'
      },
      {
        Header : () => <strong>Nom de la compagnie</strong>,
        accessor : 'offre.nomCompagnie'
      },
      {
        Header : () => <strong>Poste</strong>,
        accessor : 'offre.posteARemplir'
      }
    ]

    const { data, pages, loading } = this.state;

    return (
      <div>
        <h3>Liste des applications valides</h3>
        <p><Link to="/valideApplicationEtudiant">Liste des applications à valider</Link></p>
        <ReactTable
        	columns = {columns}
          manual
          data={data}
          pages={pages}
          defaultPageSize={10}
          loading={loading}
          onFetchData={this.handleApplicationList}
        />
      </div>
    )
  }
}

export default ApplicationStage;
