import React, { Component } from 'react';
import ReactTable from 'react-table'
import axios from 'axios';
import {Link} from 'react-router-dom';
import 'react-table/react-table.css';
import "../Stage/stage.css";

class AccepteStage extends Component {
  constructor(props) {
      super(props);
      var userInfo = JSON.parse(localStorage.getItem("account"));
      this.state = {
        currUser: userInfo,
        currUserType: userInfo.type.toLowerCase(),
        currUserId: userInfo.id,
        page: 0,
        pageSize: 10,
        pages: null
      };
      this.handleApplicationToAccept = this.handleApplicationToAccept.bind(this);
  }

  async handleApplicationToAccept(state, instance) {
    axios.get('http://localhost:8080/user/page/UserApplication?userId=' + this.state.currUserId + '&page=' + this.state.page + '&size=' + this.state.pageSize)
    .then(res => {
      this.setState({ loading: true });
      const users = res.data;
      console.log(users);
      this.setState({
        data: res.data.content,
        pages: res.data.totalPages,
        loading: false
      });
    })
  }

  async handleAccepterStage(id) {
    this.setState({ loading: true });
    axios.put('http://localhost:8080/application/accepteStage?applicationId=' + id + '&userId=' + this.state.currUserId)
      .then(res => {
        console.log("réussite");
        axios.post('http://localhost:8080/stage/new?idApp=' + id + '&idUser=' + this.state.currUserId)
          .then(res => {
            console.log("réussite");
          })
      })
    this.setState({ loading: false });
    window.location = "/accepteStage";
  }

  render(){
    if(this.state.currUser.type != "Etudiant"){
      window.location = "/home";
    }

    var columns = [
      {
        Header : () => <strong>Nom de la compagnie</strong>,
        accessor : 'nomCompagnie'
      },
      {
        Header : () => <strong>Description</strong>,
        accessor : 'description'
      },
      {
        Header : () => <strong>Poste</strong>,
        accessor : 'posteARemplir'
      },{
        Header : () => <strong>Accepter le stage</strong>,
        accessor : 'id',
        Cell: ({value}) => (<button onClick={()=>{this.handleAccepterStage(value)}}>Accepter le stage</button>)
      }
    ];

    const { data, pages, loading } = this.state;

    return(
      <div className="stage">
      <h3>Liste des offres de stage à accepter</h3>
      <p><Link to="/stageEtudiant">Liste des stages</Link></p>
      <ReactTable
        columns = {columns}
        manual
        data={data}
        pages={pages}
        defaultPageSize={10}
        loading={loading}
        onFetchData={this.handleApplicationToAccept}
      />
      </div>
    )
  }
}

export default AccepteStage;
