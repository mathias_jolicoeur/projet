import React, { Component } from 'react';
import ReactTable from 'react-table'
import axios from 'axios';
import {Link} from 'react-router-dom';
import 'react-table/react-table.css';

class ValideApplicationEtudiant extends Component {
  constructor(props) {
    super(props);
    var userInfo = JSON.parse(localStorage.getItem("account"));
    this.state = {
      PATH: "http://localhost:8080/application/",
      offerId: this.props.offerId,
      currUserType: userInfo.type.toLowerCase(),
      applications: [],
      page: 0,
      pageSize: 10,
      pages: null
    };
    this.handleApplicationList = this.handleApplicationList.bind(this);
    this.handleApplicationListWithOfferId = this.handleApplicationListWithOfferId.bind(this);
    this.handleTakeStudent = this.handleTakeStudent.bind(this);
  }

  async handleApplicationList(state, instance) {
    this.setState({ loading: true });
    axios.get(this.state.PATH + 'page/applicationNotValidated?page=' + state.page + '&size=' + this.state.pageSize)
      .then(res => {
        this.setState({
          data: res.data.content,
          pages: res.data.totalPages,
          loading: false
        });
      })
  }
  async handleApplicationListWithOfferId(state, instance) {
    this.setState({ loading: true });
    axios.get(this.state.PATH + 'page/applicationsByOffers?page=' + state.page + '&size=' + state.pageSize + '&offerId=' + this.state.offerId)
      .then(res => {
        this.setState({
          data: res.data.content,
          pages: res.data.totalPages,
          loading: false
        });
      })
  }
  async handleApprouveApplication(id){
    this.setState({ loading: true });
    axios.put(this.state.PATH + 'validateApplication?id=' + id)
      .then(res => {})
    this.setState({ loading: false });
    window.location = "/valideApplicationEtudiant";
  }
  async handleDeleteApplication(id){
    this.setState({ loading: true });
    axios.delete(this.state.PATH + 'deleteApplication?id=' + id)
      .then(res => {})
    this.setState({ loading: false });
    window.location = "/valideApplicationEtudiant";
  }
  async handleTakeStudent(application){
    axios.post(`${this.state.PATH}update/${application.id}`, {
      id: application.id,
      user: application.user,
      offre: application.offre,
      applicationValide: application.applicationValide,
      isStudentTaken: true
    }).then(res => {
      alert("Student Taken");
    }).catch(error => {
    });
  }

  render(){
    var columns = [
      {
        Header : () => <strong>Email</strong>,
        accessor : 'user.email',
        Cell: (props) => (<Link to={`/userDetail/${props.original.user.id}`}>{props.original.user.email}</Link>)
      },
      {
        Header : () => <strong>Nom de la compagnie</strong>,
        accessor : 'offre.nomCompagnie'
      },
      {
        Header : () => <strong>Poste</strong>,
        accessor : 'offre.posteARemplir'
      }
    ];
    switch(this.state.currUserType){
      case "compagnie":
        columns.push({
          Header : () => <strong>Accepter l'étudiant</strong>,
          accessor : "id",
          Cell: (props) => (<button onClick={()=>{this.handleTakeStudent({
                  id:props.original.id,
                  user: props.original.user,
                  offre: props.original.offre,
                  applicationValide: props.original.applicationValide,
                  studentTaken: props.original.studentTaken})}}>Prendre l'étudiant</button>)
        });
        break;
      case "coordonateur":
      columns.push(  {
          Header : () => <strong>Valider l'application</strong>,
          accessor : 'id',
          Cell: ({value}) => (<button onClick={()=>{this.handleApprouveApplication(value)}}>Approuver</button>)
        },
        {
          Header : () => <strong>Retirer l'application</strong>,
          accessor : 'id',
          Cell: ({value}) => (<button onClick={()=>{this.handleDeleteApplication(value)}}>Retirer</button>)
        });
        break
    }
    const { data, pages, loading } = this.state;

    return (
      <div>
        <h3>Liste des applications sur les offres à valider</h3>
        <p><Link to="/applicationStage">Liste des applications valides</Link></p>
        <ReactTable
        	columns = {columns}
          manual
          data={data}
          pages={pages}
          defaultPageSize={10}
          loading={loading}
          onFetchData={this.state.offerId ? this.handleApplicationListWithOfferId : this.handleApplicationList}
        />
      </div>
    )
  }
}

export default ValideApplicationEtudiant;
