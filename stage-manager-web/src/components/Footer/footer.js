import React, {Component} from 'react';
import "./footer.css";

class Footer extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <footer className="footer">
                <p className = "lead">Stage Manager ©2018 Tous droits réservés</p>
            </footer>
        );
    }
}

export default Footer;
