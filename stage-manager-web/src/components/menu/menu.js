import './menu.css'
import React, {Component} from 'react';
import {Link} from 'react-router-dom'

class Menu extends Component {
    constructor(props) {
        super(props);
        var userInfo = JSON.parse(localStorage.getItem("account"));
        this.state = {
          currUser: userInfo,
        };

    }

    logout(){
      localStorage.setItem("account", null);
      window.location = "/Home";
    }
    render() {
      var currUserType = this.state.currUser?this.state.currUser.type.toLowerCase():null;
        return (
            <nav>
            <p><Link to="/home">Home</Link></p>
            {!this.state.currUser ?
              <div className="currUserContainer">
                <p>Non connect&eacute;</p>
                <br/>
                <p><Link to="/Authentication/login">Se connecter</Link></p>
                <p><Link to="/Authentication/register">S'enregistrer</Link></p>
              </div>:
              <div className="auth">
                {currUserType == 'etudiant'?<p><Link to="/updateStatus">Status</Link></p>:""}
                {currUserType == 'etudiant'?<p><Link to="/upload">Ajouter mon cv</Link></p>:""}
                {currUserType == 'etudiant'?<p><Link to="/accepteStage">Stage</Link></p>:""}
                <p><Link to="/internshipOffers">Liste d'offres</Link></p>
                {currUserType == 'coordonateur' ? <p><Link to="/showUsers">Liste des utilisateurs</Link></p>:""}
                {currUserType == 'coordonateur' ? <p><Link to="/valideApplicationEtudiant">Valider les applications</Link></p>:""}
                {currUserType == 'compagnie' || currUserType == 'moniteur'?<p><Link to="/notifications">Notifications</Link></p>:""}
                <div className="currUserContainer">
                  <p><Link to={`/userDetail/${this.state.currUser.id}`}>{this.state.currUser.email}</Link></p>
                  <br/>
                  <p><a className="logout" onClick={this.logout}>Déconnecter</a></p>
                </div>
              </div>
            }

        </nav>
    );
    }
}

export default Menu;
