import React, {Component} from 'react';
import Slogan from '../Slogan/slogan';

class Home extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="Home w-50 p-3 mx-auto">
                <Slogan />
                <img src={ require('../../images/bertrand.png') } />
            </div>
        );
    }
}

export default Home;
