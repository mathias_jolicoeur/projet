import React, { Component } from 'react';
import ReactTable from 'react-table'
import axios from 'axios';
import {Link} from 'react-router-dom';
import 'react-table/react-table.css';
import "./stage.css";

class StageEtudiant extends Component {
  constructor(props) {
      super(props);
      var userInfo = JSON.parse(localStorage.getItem("account"));
      this.state = {
        currUser: userInfo,
        currUserType: userInfo.type.toLowerCase(),
        currUserId: userInfo.id,
        page: 0,
        pageSize: 10,
        pages: null
      };
      this.handleStage = this.handleStage.bind(this);
  }

  async handleStage(state, instance) {
    axios.get('http://localhost:8080/stage/page/stage?userId=' + this.state.currUserId + '&page=' + this.state.page + '&size=' + this.state.pageSize)
    .then(res => {
      this.setState({ loading: true });
      this.setState({
        data: res.data.content,
        pages: res.data.totalPages,
        loading: false
      });
    })
  }

  render(){
    if(this.state.currUser.type != "Etudiant"){
      window.location = "/home";
    }
    var columns = [
      {
        Header : () => <strong>Nom de la compagnie</strong>,
        accessor : 'nomCompagnie'
      },
      {
        Header : () => <strong>Description</strong>,
        accessor : 'description'
      },
      {
        Header : () => <strong>Poste</strong>,
        accessor : 'posteARemplir'
      },{
        Header : () => <strong>Session</strong>,
        accessor : 'sessionStage'
      }
    ];

    const { data, pages, loading } = this.state;

    return(
      <div className="stage">
        <h3>Liste des stages</h3>
        <p><Link to="/accepteStage">Liste des stages à accepter</Link></p>
        <ReactTable
          columns = {columns}
          manual
          data={data}
          pages={pages}
          defaultPageSize={10}
          loading={loading}
          onFetchData={this.handleStage}
        />
      </div>
    )
  }
}

export default StageEtudiant;
