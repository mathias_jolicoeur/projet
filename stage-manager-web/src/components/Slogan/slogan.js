import React, {Component} from 'react';

class Slogan extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <h3>Le meilleur endroit pour gérer et trouver des stages!</h3>
            </div>
        );
    }
}

export default Slogan;