import React, { Component } from 'react';
import axios from 'axios';
import { Button, FormGroup, Input } from 'reactstrap';
import { Link } from 'react-router-dom';
import Moment from 'moment';


export default class UserDetail extends Component {

    constructor(props) {
        super(props);
        var userInfo = JSON.parse(localStorage.getItem("account"));
        const userUrlID = this.props.match.params.userId;
        this.state = {
          PATH: "http://localhost:8080/user/",
          currUser: userInfo,
          id:userUrlID,
          email: '',
          password: '',
          status: '',
          interviewDate: '',
          cv: '',
          isValidated: false,
          type:'',
          evaluationForm: '',
          hourWorkedForm: '',
          isCompagnie: false,
          isCoordonateur:false,
          showFormUpload: false,
          showFormsReadMode:false,
          studentStateLabel: userInfo.actif?"Désactiver":"Activer",
          isCurrUserProfile: userUrlID === (userInfo.id+"")
        };
        this.renderValidationOptions = this.renderValidationOptions.bind(this);
      }

  componentDidMount() {
    const that = this;
    this.getUsersById(this.props.match.params.userId).then((nada) => {
      if(that.state.currUser){
        that.setState({isCompagnie:that.state.currUser.type.toLowerCase() === 'compagnie',
        isCoordonateur:that.state.currUser.type.toLowerCase() === 'coordonateur'});
        that.setState({showFormUpload: that.state.isCompagnie && that.state.status.toLowerCase() === "stage trouvé",
                      showFormsReadMode:  that.state.isCoordonateur && that.state.status.toLowerCase() === "stage trouvé"});
      } else {
        window.location = "/Authentication/login";
      }
    });
  }
  getUsersById(id){
    const that = this;
    return new Promise(
      function(resolve){
        axios.get(`${that.state.PATH}${id}`).then(res => {
          const user = res.data;
          that.setState({email: user.email,
                         password: user.password,
                         status: user.status,
                         interviewDate: user.interviewDate,
                         cv: user.cv,
                         type: user.type,
                         isValidated: user.validated,
                         evaluationForm: user.evaluationForm,
                         hourWorkedForm: user.hourWorkedForm
                        });
          resolve();
        });
      }
    );
  }
  handleSubmit (event, isValidated){
    event.preventDefault();
    this.toggleIsValidated(this.props.match.params.userId, isValidated);
  }
  toggleIsValidated(id, isValidated){
    axios.post(`${this.state.PATH}update/${id}`, {
      id: this.props.match.params.userId,
      email: this.state.email,
      password: this.state.password,
      cv: this.state.cv,
      status: this.state.status,
      interviewDate: this.state.interviewDate,
      isValidated: isValidated,
      type: this.state.type,
    }).then(res => {
      this.setState({isValidated: true})
    }).catch(error => {});
  }
  onChangeEvaluationForm = event => {
    this.setState({evaluationForm:event.target.files[0]});
  }
  onChangeHourWorkedForm = event => {
    this.setState({hourWorkedForm:event.target.files[0]});
  }
  handleSubmitFormEvaluation = event => {
    const formdata = new FormData();
    formdata.append('file', this.state.evaluationForm);
    axios.post(this.state.PATH + 'formEvaluation?userId='+this.state.id, formdata)
    .then(res => {
      this.setState({
        evaluationForm:null
      });
      window.alert("Le formulaire d'évaluation du stagiaire à été changé!");
    });
  }
  handleSubmitFormHours = event => {
    const formdata = new FormData();
    formdata.append('file', this.state.hourWorkedForm);
    axios.post(this.state.PATH + 'formHours?userId='+this.state.id, formdata)
    .then(res => {
      this.setState({
        hourWorkedForm:null
      });
      window.alert("Le formulaire des heures travaillées à été changé!");
    });
  }
  handleDownloadEvaluation = event => {
    const that = this;
    axios.get(this.state.PATH + 'downloadEvaluation?id=' + this.state.id)
      .then(res => {
        fetch("data:application/pdf;base64," + res.data)
        .then(function(resp) {return resp.blob()})
        .then(function(blob) {
          let url = window.URL.createObjectURL(blob);
          let a = document.createElement('a');
          a.href = url;
          a.download = "Evaluation.pdf";
          a.click();
          that.setState({ loading: false });
        });
      })
  }
  handleDownloadHours = event => {
    const that = this;
    axios.get(this.state.PATH + 'downloadHours?id=' + this.state.id)
      .then(res => {
        fetch("data:application/pdf;base64," + res.data)
        .then(function(resp) {return resp.blob()})
        .then(function(blob) {
          let url = window.URL.createObjectURL(blob);
          let a = document.createElement('a');
          a.href = url;
          a.download = "Heures.pdf";
          a.click();
          that.setState({ loading: false });
        });
      })
  }
  handleActivateAccount = event => {
    axios.post(this.state.PATH + 'setActive?userId='+this.state.currUser.id + '&isActive='+!this.state.currUser.actif)
    .then(res => {
      var currUser = this.state.currUser;
      currUser.actif = currUser.actif?false:true;
      this.setState({
          currUser: currUser,
          studentStateLabel: currUser.actif?"Désactiver":"Activer"
      });
      window.alert("Vous etes maintenant " +  (currUser.actif?"actif":"inactif"));
    });
  }
  renderValidationOptions(){
    if(this.state.currUser.type.toLowerCase() === "coordonateur"){
      if(this.state.isValidated){
        return   <div>
            <Button color="success" onClick={(event) => {this.handleSubmit(event, true)}}>Valider</Button>
          </div>
      } else {
        return <div>
          <Button color="danger" onClick={(event) => {this.handleSubmit(event, false)}}>Invalider</Button>
        </div>
      }
    }
    return "";
  }
  render() {
    return (
      <div className="ModifieUser w-50 p-3 mx-auto">
        <h3>Détail d'un utilisateur</h3>
        <p>Id: {this.state.id}</p>
        <p>Courriel: {this.state.email}</p>
        <p>Statut: {this.state.status}</p>
        <p>Date d'entrevue: {this.state.interviewDate}</p>
        <p>CV: {this.state.cv}</p>
        <p>CV Validé: {new Boolean (this.state.isValidated).toString()}</p>
        <p>État de l'utilisateur: {this.state.currUser.actif?"actif":"inactif"}</p>
        {this.state.showFormUpload ?<div><p>Formulaire d'&eacute;valuation:</p>
        <FormGroup>
          <Input type="file" name="fileToSend" accept=".pdf" multiple={false} onChange={this.onChangeEvaluationForm} required className = "fileInput"></Input>
        </FormGroup>
        <FormGroup>
          <Button onClick={this.handleSubmitFormEvaluation}>{"Soumettre"}</Button>
        </FormGroup>
        <p>Formulaire des heures:</p>
        <FormGroup>
          <Input type="file" name="fileToSend" accept=".pdf" multiple={false} onChange={this.onChangeHourWorkedForm} required className = "fileInput"></Input>
        </FormGroup>
        <FormGroup>
          <Button onClick={this.handleSubmitFormHours}>{"Soumettre"}</Button>
        </FormGroup></div>:""}
        {this.state.isCurrUserProfile || this.state.isCoordonateur?  <p><Button onClick={this.handleActivateAccount}>{this.state.studentStateLabel}</Button></p>:""}
        {this.state.showFormsReadMode ?<div><p>Formulaire d'&eacute;valuation:</p>
        <p><Button onClick={this.handleDownloadEvaluation}>&Eacute;valuation du stagiare</Button></p>
        <p>Formulaire des heures:</p>
        <p><Button onClick={this.handleDownloadHours}>Formulaire des heures</Button></p></div>:""}
        {this.renderValidationOptions}
        <Link to={`../home`}><Button color="secondary">Retour</Button></Link>
      </div>
    );
  }
}
