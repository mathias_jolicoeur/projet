import React, { Component } from 'react';
import ReactTable from 'react-table'
import axios from 'axios';
import {Link} from 'react-router-dom';
import 'react-table/react-table.css';
import { Button } from 'reactstrap';

class ShowUsers extends Component {
  constructor() {
    super();
    var userInfo = JSON.parse(localStorage.getItem("account"));
    this.state = {
      PATH:"http://localhost:8080/user/",
      currUser: userInfo,
      isCoordonateur: userInfo.type.toLowerCase() === "coordonateur",
      users: [],
      page: 0,
      pageSize: 10,
      pages: null
    };
    this.handleUsersList = this.handleUsersList.bind(this);
    this.updateAllActifUserToInactif = this.updateAllActifUserToInactif.bind(this);
  }

  async handleUsersList(state) {
    this.setState({ loading: true });
    axios.get(this.state.PATH + 'page/userNotValidated?page=' + state.page + '&size=' + state.pageSize)
      .then(res => {
        this.setState({
          data: res.data.content,
          pages: res.data.totalPages,
          loading: false
        });
      })
  }
  async handleApprouveCV(id){
    this.setState({ loading: true });
    axios.put(this.state.PATH + 'validateUser?id=' + id)
      .then(res => {})
    this.setState({ loading: false });
    window.location = "/showUsers";
  }
  async handleDownloadCV(id){
    const that=this;
    this.setState({ loading: true });
    axios.get(this.state.PATH + 'downloadCV?id=' + id)
      .then(res => {
        fetch("data:application/pdf;base64," + res.data)
        .then(function(resp) {return resp.blob()})
        .then(function(blob) {
          let url = window.URL.createObjectURL(blob);
          let a = document.createElement('a');
          a.href = url;
          a.download = "CV.pdf";
          a.click();
          that.setState({ loading: false });
        });
      })

  }
  updateAllActifUserToInactif(){
    axios.post(this.state.PATH + 'setAllInactif')
      .then(res => {
        this.handleUsersList(this.state);
        window.alert("Tous les utilisateur sont maintenant inactif");
      })
  }
  render(){
    const columns = [
      {
        Header : () => <strong>ID</strong>,
        accessor : 'id',
        Cell: (props) => (<Link to={`/userDetail/${props.original.id}`}>{props.original.id}</Link>)
      },{
        Header : () => <strong>Email</strong>,
        accessor : 'email',
      },
      {
        Header : () => <strong>Type</strong>,
        accessor : 'type'
      },
      {
        Header: () => <strong>CV</strong>,
        accessor: 'id',
        Cell:  (props) => (props.original.cv!=null?<button onClick={()=>{this.handleDownloadCV(props.original.id)}}>Télécharger</button>:"Aucun CV")
      },
      {
        Header : () => <strong>Valider CV</strong>,
        accessor : 'id',
        Cell: ({value}) => (<button onClick={()=>{this.handleApprouveCV(value)}}>Approuver</button>)
      },
      {
        Header: () => <strong>Statut de stage</strong>,
        accessor: 'status'
      },
      {
        Header : () => <strong>État du compte</strong>,
        accessor : 'actif',
        Cell: ({value}) => (<p>{value?"Actif":"Inactif"}</p>)
      }
    ]

    const { data, pages, loading } = this.state;

    return (
      <div>
        <h3>Liste des utilisateurs</h3>
        <p><Link to="/user">Liste des étudiants ayant des CV valides</Link></p>
        {this.state.isCoordonateur?<p><Button onClick={this.updateAllActifUserToInactif}>D&eacute;sactiver tout les &eacute;tudiants</Button></p>:""}
        <ReactTable
        	columns = {columns}
          manual
          data={data}
          pages={pages}
          defaultPageSize={10}
          loading={loading}
          onFetchData={this.handleUsersList}
        />
      </div>
    )
  }
}

export default ShowUsers;
