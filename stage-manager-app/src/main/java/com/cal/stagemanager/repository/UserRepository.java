package com.cal.stagemanager.repository;

import com.cal.stagemanager.domaine.ProjectionUserApplication;
import com.cal.stagemanager.domaine.User;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface UserRepository extends JpaRepository<User,Integer>{
    User findByEmailAndPassword(String email, String password);
    User findById(int id);
    Page<User> findByType(String type, Pageable pageable);
    Page<User> findAll(Pageable pageable);
	List<User> findByIsValidated(boolean isValidated);
	Page<User> findByIsValidatedFalse(Pageable pageable);
	Page<User> findByIsValidatedTrue(Pageable pageable);
	
	@Query("SELECT " +
			 " new com.cal.stagemanager.domaine.ProjectionUserApplication(a.id, o.nomCompagnie, o.posteARemplir, o.description, a.isStudentAccepted) " +
			 "FROM User u LEFT JOIN u.applications a LEFT JOIN a.offre o " +
			 "WHERE u.id = :userId AND a.isStudentAccepted = false")
	Page<ProjectionUserApplication> findByQueryApplication(@Param("userId") int userId, Pageable pageable);
	
	@Modifying
	@Transactional
	@Query("UPDATE User user SET user.isActif = false WHERE user.isActif = true")
	int updateAllActifUserToInactif();
}