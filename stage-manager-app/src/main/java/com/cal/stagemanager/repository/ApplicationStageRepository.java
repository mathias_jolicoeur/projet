package com.cal.stagemanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.cal.stagemanager.domaine.ApplicationStage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ApplicationStageRepository extends JpaRepository<ApplicationStage, Integer>{
	ApplicationStage findById(int id);
	Page<ApplicationStage> findAll(Pageable pageable);
	Page<ApplicationStage> findByapplicationValideFalse(Pageable pageable);
	Page<ApplicationStage> findByapplicationValideTrue(Pageable pageable);
	Page<ApplicationStage> findByapplicationValideTrueAndOffre_Id(int id, Pageable pageable);
}
