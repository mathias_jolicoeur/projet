package com.cal.stagemanager.repository;

import com.cal.stagemanager.domaine.OffreStage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OffreStageRepository extends JpaRepository<OffreStage,Integer>{
    OffreStage findById(int id);
    Page<OffreStage> findByIsValidated(boolean isValidated, Pageable pageable);
    Page<OffreStage> findAll(Pageable pageable);
    Page<OffreStage> findByIsValidatedAndSession(boolean isValidated, String session, Pageable pageable);
	Page<OffreStage> findBySession(String sessionName, Pageable pageable);
	Page<OffreStage> findBySessionAndCreatedByIdAndIsValidated(String sessionName, int createdById, boolean isValidated, Pageable pageable);
	Page<OffreStage> findByCreatedById(int createdById, Pageable pageable);
}