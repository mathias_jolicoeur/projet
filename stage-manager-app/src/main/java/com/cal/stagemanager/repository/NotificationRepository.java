package com.cal.stagemanager.repository;

import com.cal.stagemanager.domaine.Notification;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NotificationRepository extends JpaRepository<Notification,Integer>{
    Page<Notification> findAll(Pageable pageable);
    Page<Notification> findBySessionAndUserId(String session, int userId, Pageable pageable);
}