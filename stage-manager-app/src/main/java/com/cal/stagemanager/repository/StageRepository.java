package com.cal.stagemanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.cal.stagemanager.domaine.ProjectionStage;
import com.cal.stagemanager.domaine.Stage;

public interface StageRepository extends JpaRepository<Stage, Integer>{

	Stage findById(int id);
	
	@Query("SELECT" +
			" new com.cal.stagemanager.domaine.ProjectionStage(s.id, o.nomCompagnie, o.posteARemplir, o.description, o.session) " + 
			"FROM Stage s LEFT JOIN s.stage o LEFT JOIN s.stagiaire u " + 
			"WHERE u.id = :userId")
	Page<ProjectionStage> findByQueryStage(@Param("userId") int userId, Pageable pageable);
}
