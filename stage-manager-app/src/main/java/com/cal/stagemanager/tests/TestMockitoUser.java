package com.cal.stagemanager.tests;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.cal.stagemanager.domaine.ProjectionUserApplication;
import com.cal.stagemanager.domaine.User;
import com.cal.stagemanager.repository.UserRepository;
import com.cal.stagemanager.service.UserService;

@RunWith(MockitoJUnitRunner.Silent.class)
public class TestMockitoUser {

	@Mock // Collaborateurs
	private UserRepository userRepo;
	// Page et pageable ?
	// Projection ?

	@InjectMocks // classe sous test
	private UserService userService;

	User user01;
	List<User> liste01;
	Page<User> page;
	Page<ProjectionUserApplication> page2;
	Pageable pageable;

	@Before
	public void setUp() throws Exception {
		user01 = new User();
		user01.setId(0);
		user01.setEmail("test@test.com");
		user01.setPassword("test");
		user01.setType("Etudiant");
		user01.setStatus("test");
		user01.setIsValidated(true);
		liste01 = new ArrayList<User>();
		liste01.add(user01);
		page = new Page<User>() {

			@Override
			public int getNumber() {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public int getSize() {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public int getNumberOfElements() {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public List<User> getContent() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public boolean hasContent() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public Sort getSort() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public boolean isFirst() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean isLast() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean hasNext() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean hasPrevious() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public Pageable nextPageable() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Pageable previousPageable() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Iterator<User> iterator() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public int getTotalPages() {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public long getTotalElements() {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public <U> Page<U> map(Function<? super User, ? extends U> converter) {
				// TODO Auto-generated method stub
				return null;
			}
		};
		pageable = new Pageable() {

			@Override
			public Pageable previousOrFirst() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Pageable next() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public boolean hasPrevious() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public Sort getSort() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public int getPageSize() {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public int getPageNumber() {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public long getOffset() {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public Pageable first() {
				// TODO Auto-generated method stub
				return null;
			}
		};
	}

	@After
	public void tearDown() throws Exception {
		user01 = null;
		liste01 = null;
		page = null;
		pageable = null;
	}

	@Test
	public void testCreateUser() {
		userService.createUser(user01);
		verify(userRepo).save(any());
	}
	
	@Test
	public void testFindAll() {
		// Arrange
		when(userRepo.findAll()).thenReturn(liste01);
		// Act
		List<User> users = userService.findAll();
		// Assert
		assertThat(users).contains(user01);
		verify(userRepo).findAll();
	}

	@Test
	public void testFindById() {
		when(userRepo.findById(user01.getId())).thenReturn(user01);

		User userFound = userService.findById(user01.getId());

		assertThat(userFound).isEqualTo(user01);
		verify(userRepo).findById(user01.getId());
	}

	@Test
	public void testFindByEmailAndPassword() {
		when(userRepo.findByEmailAndPassword(user01.getEmail(), user01.getPassword())).thenReturn(user01);
		
		User userFound = userService.findByEmailAndPassword(user01.getEmail(), user01.getPassword());
		assertThat(userFound).isEqualTo(user01);
		verify(userRepo).findByEmailAndPassword(user01.getEmail(), user01.getPassword());
	}

	@Test
	public void testFindAllPage() {
		when(userRepo.findAll(pageable)).thenReturn(page);
		Page<User> pageFound = userService.findAllPage(0, 1);
		assertThat(pageFound).isEqualTo(null);
	}
	
	//find how to deal with findById in the function
	@Ignore
	public void testUpdateUser() {
		user01.setPassword("passwd updated");
		userService.updateUser(user01.getId(), user01);
		verify(userRepo).save(any());
	}
	
	@Test
	public void testfindByType() {
		when(userRepo.findByType(user01.getType(), pageable)).thenReturn(page);
		Page<User> pageFound = userService.findByType(user01.getType(), 0, 1);
		assertThat(pageFound).isEqualTo(null);
	}

	@Test
	public void testFindUserValidated() {
		when(userRepo.findByIsValidatedTrue(pageable)).thenReturn(page);
		Page<User> pageFound = userService.findUserValidated(0, 1);
		assertThat(pageFound).isEqualTo(null);
	}
	
	@Test
	public void testFindUserNotValidated() {
		when(userRepo.findByIsValidatedFalse(pageable)).thenReturn(page);
		Page<User> pageFound = userService.findUserNotValidated(0, 1);
		assertThat(pageFound).isEqualTo(null);
	}
	
	@Ignore
	public void testSetUserValidated() {
		userService.createUser(user01);
		userService.setUserValidated(user01.getId());
		assertTrue(userService.findById(user01.getId()).isValidated());
	}
	
	
	@Test
	public void testFindByQueryApplication() {
		when(userRepo.findByQueryApplication(user01.getId(), pageable)).thenReturn(page2);
		Page<ProjectionUserApplication> pageFound = userService.findByQueryApplication(user01.getId(), 0, 1);
		assertThat(pageFound).isEqualTo(null);
	}
	
	@Ignore
	public void testUploadFile() {
		
	}
	
	@Ignore
	public void testSetCV() {
		
	}
	
	@Ignore
	public void testUploadFormHours() {
		
	}
	
	@Ignore
	public void testUploadFormEvaluation() {
		
	}
	
	@Ignore
	public void testSetHeuresTravaillees() {
		
	}
	
	@Ignore
	public void testSetEvaluationStagiare() {
		
	}
	
	@Ignore
	public void testUploadCV() {
		
	}
	
	@Ignore
	public void testSetIsActive() {
		
	}
	
	@Test
	public void testUpdateAllActifUserToInactif() {
		when(userRepo.updateAllActifUserToInactif()).thenReturn(liste01.size());
		int nbLignesModifier = userService.updateAllActifUserToInactif();
		assertThat(nbLignesModifier).isEqualTo(liste01.size());
		verify(userRepo).updateAllActifUserToInactif();
	}

}
