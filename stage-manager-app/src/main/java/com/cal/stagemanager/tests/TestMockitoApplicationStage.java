package com.cal.stagemanager.tests;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
  import org.junit.runner.RunWith;
  import org.mockito.InjectMocks;
  import org.mockito.Mock;
  import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.cal.stagemanager.domaine.ApplicationStage;
import com.cal.stagemanager.domaine.OffreStage;
import com.cal.stagemanager.domaine.User;
import com.cal.stagemanager.repository.ApplicationStageRepository;
  import com.cal.stagemanager.service.ApplicationStageService;

  @RunWith(MockitoJUnitRunner.class)
  public class TestMockitoApplicationStage {

  	@Mock
  	private ApplicationStageRepository applicationRepo;
	
  	@InjectMocks
  	private ApplicationStageService applicationService;
  	
  	final static int EMPTY =0;
  	
  	ApplicationStage applicationStage;
  	List<ApplicationStage> applications;
  	Page<ApplicationStage> page;
  	Pageable pageable;
  	OffreStage offre;
  	User user;
	
  	@Before
  	public void setUp() throws Exception {
  		applicationStage = new ApplicationStage();
  		
  		applicationStage.setApplicationValide(true);
  		applicationStage.setOffre(offre);
  		applicationStage.setStudentAccepted(true);
  		applicationStage.setStudentTaken(true);
  		applicationStage.setUser(user);
  		
  		
  		offre = new OffreStage();
  		offre.setNomCompagnie("Compagnie");
  		offre.setCreatedById(1);
  		
  		
  		user = new User();
  		
		user.setEmail("test@test.com");
		user.setPassword("test");
		user.setType("Etudiant");
		user.setStatus("test");
		user.setIsValidated(true);
		
		applicationRepo.save(applicationStage);
  	}

  	@After
  	public void tearDown() throws Exception {
  		applicationStage = null;
  		offre = null;
  		user = null;
  		applicationRepo = null;
  		applicationService = null;
  		page = null;
  		pageable = null;
  	}
	
//  	@Test
//  	public void testFindApplicationValidated() {
//		
//  	}
//	
//  	@Test
//  	public void testFindApplicationNotValidated() {
//		
//  	}
//	
//  	@Test
//  	public void testSetApplicationValidated() {
//		
//  	}
	
  	@Test
  	public void testDeleteApplication() {
  		applicationService.deleteApplication(applicationStage.getId());
		
		assertTrue(applicationRepo.count() == EMPTY);
  	}
	
//  	@Test
//  	public void testFindByOfferId() {
//  		when(applicationRepo.findById(applicationStage.getId())).thenReturn(applicationStage);
//
//		ApplicationStage as = applicationService.findByOfferId(1, 1, offre.getId());
//
//		assertThat(as).isEqualTo(applicationStage);
//		verify(applicationRepo).findById(applicationStage.getId());
//  	}
//	
  	@Test
  	public void testUpdateApplication() {
		applicationStage.setApplicationValide(false);
		applicationService.updateApplication(applicationStage.getId(), applicationStage);
		
		ApplicationStage as = applicationRepo.findById(applicationStage.getId());
		System.out.println(as.isApplicationValide());
		
		assertTrue(!as.isApplicationValide());
  	}
//	
  	@Test
  	public void testAccepteStage() {
  		ApplicationStage as = applicationRepo.findById(applicationStage.getId());
		assertTrue(as.isStudentAccepted());
  	}
 

	@Test
	public void testFindApplicationValidated() {
		when(applicationRepo.findByapplicationValideTrue(pageable)).thenReturn(page);
		Page<ApplicationStage> pageFound = applicationService.findApplicationValidated(0, 1);
		assertThat(pageFound).isEqualTo(null);
	}

	@Test
	public void testFindApplicationNotValidated() {
		when(applicationRepo.findByapplicationValideFalse(pageable)).thenReturn(page);
		Page<ApplicationStage> pageFound = applicationService.findApplicationNotValidated(0, 1);
		assertThat(pageFound).isEqualTo(null);
	}

	@Ignore
	public void testSetApplicationValidated() {

	}

	@Test
	public void testFindByOfferId() {
		when(applicationRepo.findByapplicationValideTrueAndOffre_Id(applicationStage.getId(), pageable)).thenReturn(page);
		Page<ApplicationStage> pageFound = applicationService.findByOfferId(0, 1, applicationStage.getId());
		assertThat(pageFound).isEqualTo(null);
	}
	
}
