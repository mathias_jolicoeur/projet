package com.cal.stagemanager.tests;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.doReturn;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.cal.stagemanager.domaine.ApplicationStage;
import com.cal.stagemanager.domaine.OffreStage;
import com.cal.stagemanager.domaine.ProjectionStage;
import com.cal.stagemanager.domaine.Stage;
import com.cal.stagemanager.domaine.User;
import com.cal.stagemanager.repository.ApplicationStageRepository;
import com.cal.stagemanager.repository.OffreStageRepository;
import com.cal.stagemanager.repository.StageRepository;
import com.cal.stagemanager.repository.UserRepository;
import com.cal.stagemanager.service.StageService;

@RunWith(MockitoJUnitRunner.Silent.class)
public class TestMockitoStage {

	@Mock // Collaborateurs
	private StageRepository stageRepository;
	@Mock
	private UserRepository userRepository;
	@Mock
	private ApplicationStageRepository appStageRepository;
	@Mock
	private OffreStageRepository offreStageRepository;
	
	@InjectMocks // classe sous test
	private StageService stageService;
	
	Stage stage01;
	List<Stage> liste01;
	OffreStage offreStage;
	User user;
	Page<ProjectionStage> page;
	Pageable pageable;
	ProjectionStage projectionStage;
	ApplicationStage application;
	
	@Before
	public void setUp() throws Exception {
		
		offreStage = new OffreStage();
		offreStage.setCreatedById(10000000);
		offreStage.setDescription("test");
		offreStage.setIsValidated(true);
		offreStage.setNomCompagnie("compagnieTest");
		offreStage.setPosteARemplir("testPoste");
		offreStage.setSalaire(10.00);
		offreStage.setSession("testSession");
		offreStage.setUsersApplied(null);
		
		application = new ApplicationStage();
		application.setApplicationValide(true);
		application.setOffre(offreStage);
		application.setStudentAccepted(true);
		application.setStudentTaken(false);
		application.setUser(user);
		
		user = new User();
		user.setActif(true);
		user.setApplications(null);
		user.setCv(null);
		user.setEmail("test@test.com");
		user.setEvaluationForm(null);
		user.setHourWorkedForm(null);
		user.setInterviewDate(null);
		user.setIsValidated(true);
		user.setPassword("test");
		user.setStage(null);
		user.setStatus("test");
		user.setType("test");
		user.setValidated(true);
		
		stage01 = new Stage();
		stage01.setId(stage01.getId());
		stage01.setSessionStage("Hiv2018");
		stage01.getSessionStage();
		stage01.setStage(offreStage);
		stage01.getStage();
		stage01.setStagiaire(user);
		stage01.getStagiaire();
		stage01.toString();
		liste01 = new ArrayList<Stage>();
		liste01.add(stage01);
		
		page = new Page<ProjectionStage>() {

			@Override
			public int getNumber() {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public int getSize() {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public int getNumberOfElements() {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public List<ProjectionStage> getContent() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public boolean hasContent() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public Sort getSort() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public boolean isFirst() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean isLast() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean hasNext() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean hasPrevious() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public Pageable nextPageable() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Pageable previousPageable() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Iterator<ProjectionStage> iterator() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public int getTotalPages() {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public long getTotalElements() {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public <U> Page<U> map(Function<? super ProjectionStage, ? extends U> converter) {
				// TODO Auto-generated method stub
				return null;
			}
			
		};

		pageable = new Pageable() {
			
			@Override
			public Pageable previousOrFirst() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Pageable next() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public boolean hasPrevious() {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public Sort getSort() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public int getPageSize() {
				// TODO Auto-generated method stub
				return 0;
			}
			
			@Override
			public int getPageNumber() {
				// TODO Auto-generated method stub
				return 0;
			}
			
			@Override
			public long getOffset() {
				// TODO Auto-generated method stub
				return 0;
			}
			
			@Override
			public Pageable first() {
				// TODO Auto-generated method stub
				return null;
			}
		};
		
		projectionStage = new ProjectionStage(stage01.getId(), offreStage.getNomCompagnie(), offreStage.getPosteARemplir(), offreStage.getDescription(), offreStage.getSession());
		projectionStage.setId(projectionStage.getId());
		projectionStage.setNomCompagnie(projectionStage.getNomCompagnie());
		projectionStage.setPosteARemplir(projectionStage.getPosteARemplir());
		projectionStage.setDescription(projectionStage.getDescription());
		projectionStage.setSessionStage(projectionStage.getSessionStage());
	}

	@After
	public void tearDown() throws Exception {
		stage01 = null;
		liste01 = null;
		offreStage = null;
		user = null;
		page = null;
		pageable = null;
		projectionStage = null;
	}

	@Test
	public void testFindAll() {
		//Arrange
		when(stageRepository.findAll()).thenReturn(liste01);
		//Act
		List<Stage> stages = stageService.findAll();
		//Assert
		assertThat(stages).isEqualTo(liste01);
		verify(stageRepository).findAll();
	}

	@Test
	public void testFindById() {
		when(stageRepository.findById(stage01.getId())).thenReturn(stage01);
		
		Stage stageFound = stageService.findById(stage01.getId());
		
		assertThat(stageFound).isEqualTo(stage01);
		verify(stageRepository).findById(stage01.getId());
	}
	
	@Test
	public void testfindByQuery() {
		when(stageRepository.findByQueryStage(user.getId(), pageable)).thenReturn(page);
		
		Page<ProjectionStage> pageFound = stageService.findByQueryStage(user.getId(), 0, 1);
		
		assertThat(pageFound).isEqualTo(null);
		//verify(stageRepository).findByQueryStage(user.getId(), pageable);
	}
	
	@Test
	public void testCreateStage() {
		when(appStageRepository.findById(application.getId())).thenReturn(application);
		when(offreStageRepository.findById(application.getOffre().getId())).thenReturn(offreStage);
		when(userRepository.findById(user.getId())).thenReturn(user);
		stageService.createStage(application.getId(), user.getId());
		verify(stageRepository).save(any());
	}
}
