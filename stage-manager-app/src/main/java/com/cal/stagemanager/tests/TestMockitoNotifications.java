package com.cal.stagemanager.tests;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.cal.stagemanager.domaine.Notification;
import com.cal.stagemanager.domaine.User;
import com.cal.stagemanager.repository.NotificationRepository;
import com.cal.stagemanager.service.NotificationService;

@RunWith(MockitoJUnitRunner.Silent.class)
public class TestMockitoNotifications {

	final static int EMPTY = 0;

	@Mock // Collaborateur
	private NotificationRepository notificationRepository;

	@InjectMocks // classe sous test
	private NotificationService notificationService;

	Notification notification;
	User user01;

	List<Notification> notifications;
	Page<Notification> pageNotifications;
	Pageable pageable;

	@Before
	public void setUp() throws Exception {
		notification = new Notification();

		notification.setCompagnie("Compagnie01");
		notification.setDate("27/01/18");
		notification.setNomStagiaire("Andrew");
		notification.setPosteARemplir("Poste01");
		notification.setSession("A2018");
		notification.setUserId(0);
		notifications = new ArrayList<>();
		notifications.add(notification);

		user01 = new User();
		user01.setId(0);
		user01.setEmail("test@test.com");
		user01.setPassword("test");
		user01.setType("Etudiant");
		user01.setStatus("test");
		user01.setIsValidated(true);

		pageNotifications = new Page<Notification>() {

			@Override
			public int getNumber() {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public int getSize() {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public int getNumberOfElements() {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public List<Notification> getContent() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public boolean hasContent() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public Sort getSort() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public boolean isFirst() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean isLast() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean hasNext() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean hasPrevious() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public Pageable nextPageable() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Pageable previousPageable() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Iterator<Notification> iterator() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public int getTotalPages() {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public long getTotalElements() {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public <U> Page<U> map(Function<? super Notification, ? extends U> converter) {
				// TODO Auto-generated method stub
				return null;
			}
		};

	}

	@Test
	public void testCreateNotification() {
		notificationService.createNotification(notification);
		verify(notificationRepository).save(any());
	}

	@Test
	public void testDeleteNotification() {
		notificationService.createNotification(notification);
		notificationService.deleteNotification(notification.getId());

		assertTrue(notificationRepository.count() == EMPTY);

	}

	@Test
	public void testFindBySessionAndUserId() {

		when(notificationRepository.findBySessionAndUserId("A2018", user01.getId(), pageable))
				.thenReturn(pageNotifications);

		Page<Notification> pageFound = notificationService.findBySessionAndUserId("A2018", user01.getId(), 0, 1);

		assertThat(pageFound).isEqualTo(null);

	}

	@Test
	public void testGetAllNotifications() {

		when(notificationRepository.findAll(pageable))
				.thenReturn(pageNotifications);

		Page<Notification> pageFound = notificationService.findAllPage(0, 1);

		assertThat(pageFound).isEqualTo(null);

	}

	@After
	public void tearDown() throws Exception {
		notification = null;
		notifications = null;
		pageNotifications = null;
		pageable = null;
	}

}
