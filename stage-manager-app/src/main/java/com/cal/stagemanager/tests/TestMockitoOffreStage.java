package com.cal.stagemanager.tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import static org.assertj.core.api.Assertions.assertThat;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.any;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;
import java.util.function.Function;

import javax.print.attribute.HashAttributeSet;

import com.cal.stagemanager.domaine.OffreStage;
import com.cal.stagemanager.domaine.User;
import com.cal.stagemanager.repository.OffreStageRepository;
import com.cal.stagemanager.repository.UserRepository;
import com.cal.stagemanager.service.OffreStageService;
@RunWith(MockitoJUnitRunner.class)
public class TestMockitoOffreStage {

	@Mock
	private OffreStageRepository offreRepo;

	@InjectMocks
	private OffreStageService offreService;

	OffreStage offre01;
	User user01;
	List<OffreStage> offres;
	Page<OffreStage> page;
	Pageable pageable;

	@Before
	public void setUp() throws Exception {
		offre01 = new OffreStage();
		offre01.setDescription("description01");
		offre01.setNomCompagnie("compagnie01");
		offre01.setPosteARemplir("poste01");
		offre01.setSalaire(6.66);
		offre01.setIsValidated(true);
		offre01.setSession("automne 2018");
		offre01.setApplications(new TreeSet<>());
		offre01.setDateDepart("01-01-2018");
		offres = new ArrayList<>();
		offres.add(offre01);
		
		user01 = new User();
		user01.setEmail("test@test.com");
		user01.setPassword("test");
		user01.setType("Etudiant");
		user01.setStatus("test");
		user01.setIsValidated(true);

		page = new Page<OffreStage>() {

			@Override
			public int getNumber() {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public int getSize() {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public int getNumberOfElements() {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public List<OffreStage> getContent() {
				 //TODO Auto-generated method stub
				return null;
			}

			@Override
			public boolean hasContent() {
				 //TODO Auto-generated method stub
				return false;
			}

			@Override
			public Sort getSort() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public boolean isFirst() {
				 //TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean isLast() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean hasNext() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean hasPrevious() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public Pageable nextPageable() {
				 //TODO Auto-generated method stub
				return null;
			}

			@Override
			public Pageable previousPageable() {
				 //TODO Auto-generated method stub
				return null;
			}

			@Override
			public Iterator<OffreStage> iterator() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public int getTotalPages() {
				 //TODO Auto-generated method stub
				return 0;
			}

			@Override
			public long getTotalElements() {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public <U> Page<U> map(Function<? super OffreStage, ? extends U> converter) {
				// TODO Auto-generated method stub
				return null;
			}
		};
		pageable = new Pageable() {

			@Override
			public int getPageNumber() {
				 //TODO Auto-generated method stub
				return 0;
			}

			@Override
			public int getPageSize() {
				 //TODO Auto-generated method stub
				return 0;
			}

			@Override
			public long getOffset() {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public Sort getSort() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Pageable next() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Pageable previousOrFirst() {
				 //TODO Auto-generated method stub
				return null;
			}

			@Override
			public Pageable first() {
				 //TODO Auto-generated method stub
				return null;
			}

			@Override
			public boolean hasPrevious() {
				 //TODO Auto-generated method stub
				return false;
			}
		};
	}

	@After
	public void tearDown() throws Exception {
		offre01 = null;
		page = null;
		pageable = null;
		offres = null;
		user01 = null;
	}

	@Test
	public void testFindAll() {
		when(offreRepo.findAll()).thenReturn(offres);
		// Act
		Page<OffreStage> offres = offreService.findAll(0, 10);
		// Assert
		assertThat(offres).isEqualTo(null);
	}

	@Test
	public void testfindById() {
		when(offreRepo.findById(1)).thenReturn(offre01);
		OffreStage offreTrouvee = offreRepo.findById(1);
		assertThat(offreTrouvee).isEqualTo(offre01);
	}
	
	@Test
	public void testFindByIsValidated() {
		when(offreRepo.findByIsValidated(offre01.isValidated(), pageable)).thenReturn(page);
		Page<OffreStage> pageFound = offreService.findByIsValidated(offre01.isValidated(), 0, 1);
		assertThat(pageFound).isEqualTo(null);
	}

	@Test
	public void testFindBySessionAndCreatedByIdAndIsValidated() {
		when(offreRepo.findBySessionAndCreatedByIdAndIsValidated("automne 2018", 1, true, pageable)).thenReturn(page);
		// Act
		Page<OffreStage> offres = offreService.findBySessionAndCreatedByIdAndIsValidated("automne 2018", 1, true,0, 10);
		// Assert
		assertThat(offres).isEqualTo(null);
		//verify(offreRepo).findBySessionAndCreatedByIdAndIsValidated("automne 2018", 1, true, 0, 10);
	}

	@Test
	public void testFindByIsValidatedAndSession() {
		when(offreRepo.findByIsValidatedAndSession(true, "automne 2018", pageable)).thenReturn(page);
		 //Act
		Page<OffreStage> offres = offreService.findByIsValidatedAndSession(true, "automne 2018", 0, 10);
		// Assert
		assertThat(offres).isEqualTo(null);
		//verify(offreRepo).findByIsValidatedAndSession(true, "automne 2018", 0, 10);
	}
	
	@Test
	public void testFindAllOffers() {
		when(offreRepo.findAll(pageable)).thenReturn(page);
		Page<OffreStage> pageFound = offreService.findAllOffers(0, 1);
		assertThat(pageFound).isEqualTo(null);
	}
	
	@Test
	public void testFindBySession() {
		when(offreRepo.findBySession(offre01.getSession(), pageable)).thenReturn(page);
		Page<OffreStage> pageFound = offreService.findBySession(offre01.getSession(), 0, 1);
		assertThat(pageFound).isEqualTo(null);
	}
	
	@Test
	public void testFindByCreatedById() {
		when(offreRepo.findByCreatedById(offre01.getCreatedById(), pageable)).thenReturn(page);
		Page<OffreStage> pageFound = offreService.findBySession(offre01.getCreatedById(), 0, 1);
		assertThat(pageFound).isEqualTo(null);
	}
	
	@Test
	public void testFindBySessionAndIsCreatedByIdAndIsValidated() {
		when(offreRepo.findBySessionAndCreatedByIdAndIsValidated(offre01.getSession(), offre01.getCreatedById(), offre01.isValidated(), pageable)).thenReturn(page);
		Page<OffreStage> pageFound = offreService.findBySessionAndCreatedByIdAndIsValidated(offre01.getSession(), offre01.getCreatedById(), offre01.isValidated(), 0, 1);
		assertThat(pageFound).isEqualTo(null);
	}

	@Test
	public void testCreateOffreStage() {
		offreService.createOffreStage(offre01);
		 verify(offreRepo).save(any());
	}

	@Test
	public void testUpdateOffer() {
		String DESCRIPTION_NOT_EXPECTED = "description01";
		//when(offreRepo.findById(offre01.getId()).thenReturn(offre01);
		offreService.createOffreStage(offre01);
		offre01.setDescription("description updated");
		offreService.updateIntershipOffer(offre01.getId(), offre01);
		 verify(offreRepo).save(any());
	}

	@Test
	public void testAddUserIdToList() {
		final boolean EXPECTED_USER_ADDED_VALUE = true;
		final boolean userAdded = offreService.addUserIdToList(user01.getId(), offre01.getId());
		 assertEquals(EXPECTED_USER_ADDED_VALUE, userAdded);
	}
	@Test
	public void testCreateApplication() {
		offreService.createApplication(user01.getId(), offre01.getId());
		 verify(offreRepo).save(any());
	}
}
