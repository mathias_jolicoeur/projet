package com.cal.stagemanager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.cal.stagemanager.service.StageManagerInit;

@SpringBootApplication
public class StageManagerAppApplication implements CommandLineRunner {

	@Autowired
	private StageManagerInit init;

	public static void main(String[] args) {
		SpringApplication.run(StageManagerAppApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		init.init();
	}
}
