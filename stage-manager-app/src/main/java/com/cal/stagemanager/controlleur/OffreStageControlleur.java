package com.cal.stagemanager.controlleur;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cal.stagemanager.domaine.OffreStage;
import com.cal.stagemanager.service.OffreStageService;

@RestController 
@CrossOrigin 
@RequestMapping ("/offreStage")
public class OffreStageControlleur{

    @Autowired 
    private OffreStageService offreStageService;
    
    @PostMapping("/createOffreStage")
    public void createOffreStage(@RequestBody OffreStage offreStage){
        offreStageService.createOffreStage(offreStage);
    }

    @GetMapping("/{id}")
    public OffreStage findById(@PathVariable int id){
        return offreStageService.findById(id);
    }

    @GetMapping("/")
    public ResponseEntity<Page<OffreStage>> findAll(@RequestParam(required = true) int page,
			@RequestParam(required = true) int size){
    	Page<OffreStage> offers = offreStageService.findAll(page, size);
        return ResponseEntity.ok(offers);
    }
    @GetMapping("/validated/{isValidated}")
    public ResponseEntity<Page<OffreStage>> findByIsValidated(@PathVariable Boolean isValidated,
    		@RequestParam(required = true) int page,
			@RequestParam(required = true) int size){
        Page<OffreStage> offers = offreStageService.findByIsValidated(isValidated, page, size);
        return ResponseEntity.ok(offers);
    }
    @GetMapping("/validatedAndSession")
    public ResponseEntity<Page<OffreStage>> findByIsValidatedAndSession(
    		@RequestParam(required = true) Boolean isValidated,
    		@RequestParam(required = true) String session,
    		@RequestParam(required = true) int page,
			@RequestParam(required = true) int size){ 
        Page<OffreStage> offers = offreStageService.findByIsValidatedAndSession(isValidated, session,page, size);
        return ResponseEntity.ok(offers);
    }
    @GetMapping("/session")
    public ResponseEntity<Page<OffreStage>> findBySession(
    		@RequestParam(required = true) String sessionName,
    		@RequestParam(required = true) int createdById,
    		@RequestParam(required = true) Boolean isValidated,
    		@RequestParam(required = true) int page,
    		@RequestParam(required = true) int size){
    	Page<OffreStage> offers = offreStageService.findBySessionAndCreatedByIdAndIsValidated(sessionName, createdById, isValidated, page, size);
    	System.out.println(offers);
    	return ResponseEntity.ok(offers);
    }
    @PostMapping("/update/{id}")
    public void updateOffer(@PathVariable int id, @RequestBody OffreStage internshipOffer){
        offreStageService.updateIntershipOffer(id, internshipOffer);
    }
    
    @GetMapping("/page/offres")
    public ResponseEntity<Page<OffreStage>> getAllOffers(
        @RequestParam(required=true) int page, 
        @RequestParam(required=true) int size) {
            Page<OffreStage> offers = offreStageService.findAllOffers(page, size);
            return ResponseEntity.ok(offers);
    }
    @PostMapping("/addUserId/{userId}/{offreStageId}")
    public boolean addUserIdToList(@PathVariable int userId, @PathVariable int offreStageId){
        return offreStageService.addUserIdToList(userId, offreStageId);
    }

}