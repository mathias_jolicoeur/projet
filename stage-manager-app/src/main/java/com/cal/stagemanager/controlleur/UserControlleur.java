package com.cal.stagemanager.controlleur;

import java.util.Base64;
import java.util.List;

import com.cal.stagemanager.domaine.ProjectionUserApplication;
import com.cal.stagemanager.domaine.User;
import com.cal.stagemanager.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController 
@CrossOrigin 
@RequestMapping ("/user")
public class UserControlleur{

    @Autowired 
    private UserService userService;
    
    @PostMapping("/createUser")
    public void createUser(@RequestBody User user){
        userService.createUser(user);
    }

    @GetMapping("/{id}")
    public User findById(@PathVariable int id){
        return userService.findById(id);
    }

    @GetMapping("/")
    public List<User> findAll(){
        return userService.findAll();
    }

    @PostMapping("/login")
    public User findByEmailAndPassword(@RequestBody User user){
        return userService.findByEmailAndPassword(user.getEmail(), user.getPassword());
    }
    @PostMapping("/update/{id}")
    public void updateOffer(@PathVariable int id, @RequestBody User user){
        userService.updateUser(id, user);
    }

    @GetMapping("/page/users")
    public ResponseEntity<Page<User>> getAllUsers(
    		@RequestParam(required=true) int page,
    		@RequestParam(required=true) int size) {
    	Page<User> users = userService.findUserValidated(page, size);
    	return ResponseEntity.ok(users);
    }
    @GetMapping("/page/userNotValidated")
    public ResponseEntity<Page<User>> getUserNotValidated(
        @RequestParam(required=true) int page, 
        @RequestParam(required=true) int size) {
            Page<User> users = userService.findUserNotValidated(page, size);
            return ResponseEntity.ok(users);
    }
	
	@PutMapping("/validateUser")
	public ResponseEntity<User> validateUser(
			@RequestParam(required=true) int id){
		userService.setUserValidated(id);
		return ResponseEntity.noContent().build();
	}
    
    @GetMapping("/page/type")
    public ResponseEntity<Page<User>> getUserByType(
    		@RequestParam(required=true) int page,
    		@RequestParam(required=true) int size,
    		@RequestParam(required=true) String type){
    	Page<User> users = userService.findByType(type, page, size);
    	return ResponseEntity.ok(users);
    }
    
    @GetMapping("/page/UserApplication")
    public ResponseEntity<Page<ProjectionUserApplication>> findByQueryApplication(
    		@RequestParam(required=true) int page,
    		@RequestParam(required=true) int size,
    		@RequestParam(required=true) int userId){
    	Page<ProjectionUserApplication> users = userService.findByQueryApplication(userId, page, size);
    	return ResponseEntity.ok(users);
    }
    
    @PostMapping("/createCv")
    public void uploadCV(@RequestParam(required = true) MultipartFile file, @RequestParam(required = true) String userId) {
    	userService.uploadCV(file, userId); 
    }
    
    @GetMapping("/downloadCV")
    public ResponseEntity<String> downloadCv(
    		@RequestParam(required=true) int id){
    	User user = userService.findById(id);
    	byte[] userCV = user.getCv();
    	String base64Cv = Base64.getEncoder().encodeToString(userCV);
    	return ResponseEntity.ok(base64Cv);
    }
    
    @PostMapping("/formEvaluation")
    public void uploadFormEvaluation(@RequestParam(required = true) MultipartFile file,  @RequestParam(required = true) String userId) {
    	userService.uploadFormEvaluation(file, userId); 
    }
    
    @PostMapping("/formHours")
    public void uploadFormHours(@RequestParam(required = true) MultipartFile file,  @RequestParam(required = true) String userId) {
    	userService.uploadFormHours(file, userId); 
    }
    
    @GetMapping("/downloadEvaluation")
    public ResponseEntity<String> downloadEvaluation(@RequestParam(required=true) int id) {
    	User user = userService.findById(id);
    	byte[] userEvaluation = user.getEvaluationForm();
    	String base64Evaluation = Base64.getEncoder().encodeToString(userEvaluation);
    	return ResponseEntity.ok(base64Evaluation);
    }
    
    @GetMapping("/downloadHours")
    public ResponseEntity<String> downloadHours(@RequestParam(required=true) int id) {
    	User user = userService.findById(id);
    	byte[] userHours = user.getHourWorkedForm();
    	String base64Hours = Base64.getEncoder().encodeToString(userHours);
    	return ResponseEntity.ok(base64Hours);
    }
    @PostMapping("/setActive")
    public void setActive(@RequestParam(required = true) String userId,  @RequestParam(required = true) Boolean isActive) {
    	userService.setIsActive(Integer.parseInt(userId), isActive);
    }
    @PostMapping("/setAllInactif")
    public void setAllInactif() {
    	userService.updateAllActifUserToInactif();
    }
}