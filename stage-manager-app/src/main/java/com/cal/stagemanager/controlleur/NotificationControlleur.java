package com.cal.stagemanager.controlleur;

import java.util.List;

import com.cal.stagemanager.domaine.Notification;
import com.cal.stagemanager.service.NotificationService;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController 
@CrossOrigin
@RequestMapping ("/notification")
public class NotificationControlleur{

    @Autowired 
    private NotificationService notificationService;
    
    @PostMapping("/createNotification")
    public void createNotification(@RequestBody Notification notification){
        notificationService.createNotification(notification);
    }

    @DeleteMapping("deleteNotification")
	public ResponseEntity<Notification> deleteNotification(@RequestParam(required = true) int id) {
		notificationService.deleteNotification(id);
		return ResponseEntity.noContent().build();
	}

    @GetMapping("/page/notifications")
    public ResponseEntity<Page<Notification>> getAllNotifications(
    		@RequestParam(required=true) int page,
    		@RequestParam(required=true) int size) {
    	Page<Notification> notifications = notificationService.findAllPage(page, size);
    	return ResponseEntity.ok(notifications);
    }

    @GetMapping("/notificationsSession")
    public ResponseEntity<Page<Notification>> findBySessionAndUserId(
    		@RequestParam(required = true) String session,
    		@RequestParam(required = true) int userId,
    		@RequestParam(required = true) int page,
			@RequestParam(required = true) int size){ 
        Page<Notification> notifications = notificationService.findBySessionAndUserId(session, userId,page, size);
        return ResponseEntity.ok(notifications);
    }
}