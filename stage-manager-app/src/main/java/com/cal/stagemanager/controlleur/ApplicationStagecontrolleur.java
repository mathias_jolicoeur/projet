package com.cal.stagemanager.controlleur;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cal.stagemanager.domaine.ApplicationStage;
import com.cal.stagemanager.service.ApplicationStageService;

@RestController 
@CrossOrigin 
@RequestMapping("/application")
public class ApplicationStagecontrolleur {

	@Autowired
	private ApplicationStageService applicationStageService;

	@GetMapping("/page/applications")
	public ResponseEntity<Page<ApplicationStage>> getApplicationsValide(@RequestParam(required = true) int page,
			@RequestParam(required = true) int size) {
		Page<ApplicationStage> offers = applicationStageService.findApplicationValidated(page, size);
		return ResponseEntity.ok(offers);
	}

	@GetMapping("/page/applicationNotValidated")
    public ResponseEntity<Page<ApplicationStage>> getApplicationNotValidated(
        @RequestParam(required=true) int page, 
        @RequestParam(required=true) int size) {
            Page<ApplicationStage> offers = applicationStageService.findApplicationNotValidated(page, size);
            return ResponseEntity.ok(offers);
    }
	
	@PutMapping("/validateApplication")
	public ResponseEntity<ApplicationStage> validateApplication(
			@RequestParam(required=true) int id){
		applicationStageService.setApplicationValidated(id);
		return ResponseEntity.noContent().build();
	}

	@DeleteMapping("deleteApplication")
	public ResponseEntity<ApplicationStage> deleteApplication(@RequestParam(required = true) int id) {
		applicationStageService.deleteApplication(id);
		return ResponseEntity.noContent().build();
	}

	@GetMapping("page/applicationsByOffers")
	public ResponseEntity<Page<ApplicationStage>> getApplicationByOffers(@RequestParam(required = true) int page,
			@RequestParam(required = true) int size, @RequestParam(required = true) int offerId) {
		Page<ApplicationStage> offers = applicationStageService.findByOfferId(page, size, offerId);
		return ResponseEntity.ok(offers);
	}
	
	@PostMapping("/update/{id}")
	public ResponseEntity<ApplicationStage> update(
			@PathVariable int id,
			@RequestBody ApplicationStage app){
		applicationStageService.updateApplication(id, app);
		return ResponseEntity.noContent().build();
	}
	@PutMapping("/accepteStage")
	ResponseEntity<ApplicationStage> accepteStage(
			@RequestParam(required=true) int applicationId){
		applicationStageService.accepteStage(applicationId);
		return ResponseEntity.noContent().build();
	}
}
