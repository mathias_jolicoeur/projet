package com.cal.stagemanager.controlleur;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cal.stagemanager.domaine.ProjectionStage;
import com.cal.stagemanager.domaine.Stage;
import com.cal.stagemanager.service.StageService;

@RestController 
@CrossOrigin 
@RequestMapping("/stage")
public class StageControlleur {
	
	
	@Autowired
	private StageService stageService;
	
	@GetMapping("/")
	public List<Stage> findAll(){
		return stageService.findAll();
	}
	
	@GetMapping("/{id}")
	public Stage findById(@PathVariable int id) {
		return stageService.findById(id);
	}
	
	@PostMapping("/new")
	public void createStage(@RequestParam Integer idApp, @RequestParam Integer idUser) {
		stageService.createStage(idApp, idUser);
	}
	
	@GetMapping("page/stage")
	public ResponseEntity<Page<ProjectionStage>> findByQueryStage(
			@RequestParam(required=true) int page,
    		@RequestParam(required=true) int size,
    		@RequestParam(required=true) int userId){
		Page<ProjectionStage> stages = stageService.findByQueryStage(userId, page, size);
		return ResponseEntity.ok(stages);
	}
}
