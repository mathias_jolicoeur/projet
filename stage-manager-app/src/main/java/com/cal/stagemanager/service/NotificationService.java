package com.cal.stagemanager.service;

import java.util.List;

import com.cal.stagemanager.domaine.Notification;
import com.cal.stagemanager.repository.NotificationRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class NotificationService{
    
    @Autowired 
    private NotificationRepository notificationRepository;

    public void createNotification(Notification notification){
        notificationRepository.save(notification);
    }

    public void deleteNotification(int id) {
    	notificationRepository.deleteById(id);
    }

    public Page<Notification> findAllPage(int page, int size) {
    	Pageable pageable = PageRequest.of(page, size);
        return notificationRepository.findAll(pageable);
    }

    public Page<Notification> findBySessionAndUserId(String session, int userId, int page, int size){
    	Pageable pageable = PageRequest.of(page, size);
    	return notificationRepository.findBySessionAndUserId(session, userId, pageable);
    }
}