package com.cal.stagemanager.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.cal.stagemanager.domaine.ApplicationStage;
import com.cal.stagemanager.domaine.OffreStage;
import com.cal.stagemanager.domaine.User;
import com.cal.stagemanager.repository.ApplicationStageRepository;
import com.cal.stagemanager.repository.OffreStageRepository;
import com.cal.stagemanager.repository.UserRepository;

@Service
public class OffreStageService{
    Logger log = LoggerFactory.getLogger(OffreStageService.class);
    
    @Autowired 
    private OffreStageRepository offreStageRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ApplicationStageRepository applicationRepository;

    public void createOffreStage(OffreStage offreStage){
        offreStageRepository.save(offreStage);
    }
    public void updateIntershipOffer(int id, OffreStage offreStage){
        OffreStage currOffreStage = offreStageRepository.findById(id);
        currOffreStage.setId(offreStage.getId());
        currOffreStage.setDescription(offreStage.getDescription());
        currOffreStage.setNomCompagnie(offreStage.getNomCompagnie());
        currOffreStage.setPosteARemplir(offreStage.getPosteARemplir());
        currOffreStage.setSalaire(offreStage.getSalaire());
        currOffreStage.setIsValidated(offreStage.isValidated());
        currOffreStage.setSession(offreStage.getSession());
        currOffreStage.setCreatedById(offreStage.getCreatedById());
        offreStageRepository.save(currOffreStage);
    }

    public OffreStage findById(int id){
        return offreStageRepository.findById(id);
    }

    public Page<OffreStage> findAll(int page, int size){
    	Pageable pageable = PageRequest.of(page, size);
    	return offreStageRepository.findAll(pageable);
    }
    
    public Page<OffreStage> findByIsValidated(boolean isValidated, int page, int size){
    	Pageable pageable = PageRequest.of(page, size);
    	return offreStageRepository.findByIsValidated(isValidated, pageable);
    }
    
    public Page<OffreStage> findByIsValidatedAndSession(boolean isValidated, String session, int page, int size){
    	Pageable pageable = PageRequest.of(page, size);
    	return offreStageRepository.findByIsValidatedAndSession(isValidated, session, pageable);
    }
    
    public Page<OffreStage> findAllOffers(int page, int size) {
		Pageable pageable = PageRequest.of(page, size);
        return offreStageRepository.findAll(pageable);
    }

    public void createApplication(int userId, int offreStageId) {
    	ApplicationStage application = new ApplicationStage();
    	application.setOffre(offreStageRepository.findById(offreStageId));
    	application.setUser(userRepository.findById(userId));
    	application.setApplicationValide(false);
    	applicationRepository.save(application);
    }
    public boolean addUserIdToList(int userId, int offreStageId){
        OffreStage offre = offreStageRepository.findById(offreStageId);
        if(offre == null){
            return false;
        }
        List<User> users = offre.getUsersApplied();
        //users.add(userRepository.findById(userId));
        if(users == null){
            return false;
        }
        offre.setUsersApplied(users);
        //offre = offreStageRepository.save(offre);
        createApplication(userId, offreStageId);
        return true;
    }
	public Page<OffreStage> findBySession(String sessionName, int page, int size) {
		Pageable pageable = PageRequest.of(page, size);
		return offreStageRepository.findBySession(sessionName, pageable);
	}
	public Page<OffreStage> findBySession(int createdById, int page, int size) {
		Pageable pageable = PageRequest.of(page, size);
		return offreStageRepository.findByCreatedById(createdById,  pageable);
	}
	public Page<OffreStage> findBySessionAndCreatedByIdAndIsValidated(String sessionName, int createdById, boolean isValidated, int page, int size) {
		Pageable pageable = PageRequest.of(page, size);
		return offreStageRepository.findBySessionAndCreatedByIdAndIsValidated(sessionName, createdById, isValidated, pageable);
	}

}