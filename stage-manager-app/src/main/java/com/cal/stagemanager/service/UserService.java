package com.cal.stagemanager.service;

import java.io.IOException;
import java.util.List;


import com.cal.stagemanager.domaine.ProjectionUserApplication;
import com.cal.stagemanager.domaine.User;
import com.cal.stagemanager.repository.UserRepository;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	public void createUser(User user) {
		userRepository.save(user);
	}

	public User findById(int id) {
		return userRepository.findById(id);
	}

	public User findByEmailAndPassword(String email, String password) {
		return userRepository.findByEmailAndPassword(email, password);
	}
	
    public List<User> findAll(){
        return userRepository.findAll();
    }
	
    public Page<User> findAllPage(int page, int size) {
    	Pageable pageable = PageRequest.of(page, size);
        return userRepository.findAll(pageable);
    }
    public void updateUser(int id, User user){
        User currUser = userRepository.findById(id);
        System.out.println(currUser);
        currUser.setId(user.getId());
        currUser.setEmail(user.getEmail());
        currUser.setPassword(user.getPassword());
        currUser.setStatus(user.getStatus());
        currUser.setIsValidated(user.isValidated());
        currUser.setType(user.getType());
        currUser.setInterviewDate(user.getInterviewDate());
        userRepository.save(currUser);
    }
    
    public Page<User> findByType(String type, int page, int size){
    	Pageable pageable = PageRequest.of(page, size);
        return userRepository.findByType(type, pageable);
    }
    public Page<User> findUserValidated(int page, int size){
    	Pageable pageable = PageRequest.of(page, size);
    	return userRepository.findByIsValidatedTrue(pageable);
    }
    
    public Page<User> findUserNotValidated(int page, int size){
    	Pageable pageable = PageRequest.of(page, size);
    	return userRepository.findByIsValidatedFalse(pageable);
    }
    
    public void setUserValidated(int id) {
    	User user = userRepository.getOne(id);
    	user.setIsValidated(true);
    	userRepository.save(user);
    }
    
    public Page<ProjectionUserApplication> findByQueryApplication(int userId, int page, int size) {
    	Pageable pageable = PageRequest.of(page, size);
    	return userRepository.findByQueryApplication(userId, pageable);
    }
    
    public void uploadFile(MultipartFile file, String userId) {
    	byte [] fileInByte;
    	try {
    		fileInByte = file.getBytes();
    	    int userIdInt = Integer.parseInt(userId.substring(1));
    	    setCv(fileInByte, userIdInt);
    	} catch (IOException e) {
    		e.printStackTrace();
    	}
    	
    }
    
    public void setCv(byte[] cv, int id) {
    	User user = userRepository.findById(id);
    	user.setCv(cv);
    	userRepository.save(user);
    	
    }
    
    public void uploadFormHours(MultipartFile file, String userId) {
    	byte [] fileInByte;
		try {
			fileInByte = file.getBytes();
			int userIdInt = Integer.parseInt(userId);
	       	setHeuresTravaillees(fileInByte, userIdInt);
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    public void uploadFormEvaluation(MultipartFile file, String userId) {
    	byte [] fileInByte;
		try {
			fileInByte = file.getBytes();
			int userIdInt = Integer.parseInt(userId);
	       	setEvaluationStagiaire(fileInByte, userIdInt);
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    public void setHeuresTravaillees(byte[] heuresTravaillees, int id) {
    	User user = userRepository.findById(id);
    	user.setHourWorkedForm(heuresTravaillees);
    	userRepository.save(user);
    }
    
    public void setEvaluationStagiaire(byte[] evaluationStagiaire, int id) {
    	User user = userRepository.findById(id);
    	user.setEvaluationForm(evaluationStagiaire);
    	userRepository.save(user);
    }

	public void uploadCV(MultipartFile file, String userId) {
		byte[] fileInByte;
		try {
			fileInByte = file.getBytes();
			int userIdInt = Integer.parseInt(userId.substring(1));
			setCv(fileInByte, userIdInt);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	public void setIsActive(int id, Boolean isActive) {
			User user= userRepository.findById(id);
			user.setActif(isActive);
			userRepository.save(user);
	}
	public int updateAllActifUserToInactif() {
		return userRepository.updateAllActifUserToInactif();
	}

}