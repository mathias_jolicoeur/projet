package com.cal.stagemanager.service;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cal.stagemanager.domaine.ApplicationStage;
import com.cal.stagemanager.domaine.Notification;
import com.cal.stagemanager.domaine.OffreStage;
import com.cal.stagemanager.domaine.Stage;
import com.cal.stagemanager.domaine.User;
import com.cal.stagemanager.repository.ApplicationStageRepository;
import com.cal.stagemanager.repository.NotificationRepository;
import com.cal.stagemanager.repository.OffreStageRepository;
import com.cal.stagemanager.repository.StageRepository;
import com.cal.stagemanager.repository.UserRepository;

@Service
public class StageManagerInit {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private OffreStageRepository offreRepository;
	
	@Autowired
	private ApplicationStageRepository applicationStageRepository;

	@Autowired
	private NotificationRepository notificationRepository;
	
	@Autowired
	private StageRepository stageRepository;
	
	private User user01 = new User();
	private User user02 = new User();
	private User user03 = new User();
	private User user04 = new User();
	private User user05 = new User();
	
	private User user06 = new User();
	private User user07 = new User();
	private User user08 = new User();
	private User user09 = new User();
	private User user10 = new User();
	
	private User user11 = new User();
	private User user12 = new User();
	private User user13 = new User();
	private User user14 = new User();
	private User user15 = new User();
	
	private OffreStage offre01 = new OffreStage();
	private OffreStage offre02 = new OffreStage();
	private OffreStage offre03 = new OffreStage();
	private OffreStage offre04 = new OffreStage();
	private OffreStage offre05 = new OffreStage();
	private OffreStage offre06 = new OffreStage();
	private OffreStage offre07 = new OffreStage();
	private OffreStage offre08 = new OffreStage();
	
	private ApplicationStage application01 = new ApplicationStage();
	private ApplicationStage application02 = new ApplicationStage();
	private ApplicationStage application03 = new ApplicationStage();
	private ApplicationStage application04 = new ApplicationStage();
	private ApplicationStage application05 = new ApplicationStage();

	private Notification notification1 = new Notification();
	private Notification notification2 = new Notification();
	private Notification notification3 = new Notification();
	private Notification notification4 = new Notification();
	private Notification notification5 = new Notification();
	
	Set<ApplicationStage> applications;
	
	private Stage stage;
	
	private void initUser() {
		user01.setEmail("etudiant01@gmail.com");
		user01.setPassword("test");
		user01.setType("Etudiant");
		user01.setStatus("Pas de stage");
		user01.setActif(true);
		user02.setEmail("etudiant02@gmail.com");
		user02.setPassword("test");
		user02.setType("Etudiant");
		user02.setStatus("Pas de stage");
		user02.setActif(true);
		user03.setEmail("etudiant03@gmail.com");
		user03.setPassword("test");
		user03.setType("Etudiant");
		user03.setStatus("Stage trouvé");
		user03.setActif(true);
		user04.setEmail("etudiant04@gmail.com");
		user04.setPassword("test");
		user04.setType("Etudiant");
		user04.setStatus("Pas de stage");
		user05.setEmail("etudiant05@gmail.com");
		user05.setPassword("test");
		user05.setType("Etudiant");
		user05.setStatus("Pas de stage");
		
		user06.setEmail("moniteur01@gmail.com");
		user06.setPassword("test");
		user06.setType("Moniteur");
		user07.setEmail("moniteur02@gmail.com");
		user07.setPassword("test");
		user07.setType("Moniteur");
		user08.setEmail("moniteur03@gmail.com");
		user08.setPassword("test");
		user08.setType("Moniteur");
		user09.setEmail("moniteur04@gmail.com");
		user09.setPassword("test");
		user09.setType("Moniteur");
		user10.setEmail("moniteur05@gmail.com");
		user10.setPassword("test");
		user10.setType("Moniteur");
		
		user11.setEmail("compagnie01@gmail.com");
		user11.setPassword("test");
		user11.setType("Compagnie");
		user12.setEmail("compagnie02@gmail.com");
		user12.setPassword("test");
		user12.setType("Compagnie");
		user13.setEmail("compagnie03@gmail.com");
		user13.setPassword("test");
		user13.setType("Compagnie");
		user14.setEmail("compagnie04@gmail.com");
		user14.setPassword("test");
		user14.setType("Compagnie");
		user15.setEmail("coordonateur@gmail.com");
		user15.setPassword("test");
		user15.setType("Coordonateur");
		
		userRepository.save(user01);
		userRepository.save(user02);
		userRepository.save(user03);
		userRepository.save(user04);
		userRepository.save(user05);
		
		userRepository.save(user06);
		userRepository.save(user07);
		userRepository.save(user08);
		userRepository.save(user09);
		userRepository.save(user10);
		
		userRepository.save(user11);
		userRepository.save(user12);
		userRepository.save(user13);
		userRepository.save(user14);
		userRepository.save(user15);
	}
	
	private void initOffreStage() {
		offre01.setDescription("description01");
		offre01.setNomCompagnie("compagnie01");
		offre01.setPosteARemplir("poste01");
		offre01.setSalaire(6.66);
		offre01.setIsValidated(true);
		offre01.setSession("automne 2018");
		offre01.setCreatedById(this.user11.getId());
		
		offre02.setDescription("description02");
		offre02.setNomCompagnie("compagnie02");
		offre02.setPosteARemplir("poste02");
		offre02.setSalaire(6.66);
		offre02.setIsValidated(true);
		offre02.setSession("automne 2018");
		offre02.setCreatedById(this.user11.getId());
		
		offre03.setDescription("description03");
		offre03.setNomCompagnie("compagnie03");
		offre03.setPosteARemplir("poste03");
		offre03.setSalaire(6.66);
		offre03.setIsValidated(true);
		offre03.setSession("hiver 2019");
		offre03.setCreatedById(this.user11.getId());
		
		offre04.setDescription("description04");
		offre04.setNomCompagnie("compagnie04");
		offre04.setPosteARemplir("poste04");
		offre04.setSalaire(6.66);
		offre04.setIsValidated(false);
		offre04.setSession("hiver 2019");
		offre04.setCreatedById(this.user15.getId());
		
		offre05.setDescription("description05");
		offre05.setNomCompagnie("compagnie05");
		offre05.setPosteARemplir("poste05");
		offre05.setSalaire(6.66);
		offre05.setIsValidated(false);
		offre05.setSession("hiver 2019");
		offre05.setCreatedById(this.user15.getId());
		
		offre06.setDescription("description06");
		offre06.setNomCompagnie("compagnie06");
		offre06.setPosteARemplir("poste06");
		offre06.setSalaire(13.44);
		offre06.setIsValidated(true);
		offre06.setSession("automne 2018");
		offre06.setCreatedById(this.user11.getId());
		
		offre07.setDescription("description07");
		offre07.setNomCompagnie("compagnie07");
		offre07.setPosteARemplir("poste07");
		offre07.setSalaire(66.66);
		offre07.setIsValidated(false);
		offre07.setSession("automne 2018");
		offre07.setCreatedById(this.user11.getId());
		
		offre08.setDescription("description08");
		offre08.setNomCompagnie("compagnie08");
		offre08.setPosteARemplir("poste08");
		offre08.setSalaire(66.66);
		offre08.setIsValidated(false);
		offre08.setSession("hiver 2019");
		offre08.setCreatedById(this.user11.getId());
		
		offreRepository.save(offre01);
		offreRepository.save(offre02);
		offreRepository.save(offre03);
		offreRepository.save(offre04);
		offreRepository.save(offre05);
		offreRepository.save(offre06);
		offreRepository.save(offre07);
		offreRepository.save(offre08);
	} 
	
	private void initApplicationStage() {
		
		application01.setOffre(offre01);
		application01.setUser(user01);
		application01.setApplicationValide(false);
		
		application02.setOffre(offre01);
		application02.setUser(user02);
		application02.setApplicationValide(false);
		
		application03.setOffre(offre01);
		application03.setUser(user03);
		application03.setApplicationValide(true);
		
		application04.setOffre(offre02);
		application04.setUser(user01);
		application04.setApplicationValide(true);
		
		application05.setOffre(offre02);
		application05.setUser(user02);
		application05.setApplicationValide(false);
		
		applicationStageRepository.save(application01);
		applicationStageRepository.save(application02);
		applicationStageRepository.save(application03);
		applicationStageRepository.save(application04);
		applicationStageRepository.save(application05);
		
	}

	public void initNotification(){
		notification1.setCompagnie("Compagnie01");
		notification1.setDate("14/11/18");
		notification1.setNomStagiaire(user01.getEmail());
		notification1.setSession("A2018");
		notification1.setUserId(6);
		notification1.setPosteARemplir("Programmeur");



		notification2.setCompagnie("Compagnie01");
		notification2.setDate("14/11/18");
		notification2.setNomStagiaire(user02.getEmail());
		notification2.setSession("A2017");
		notification2.setUserId(11);
		notification2.setPosteARemplir("Programmeur");


		notification3.setCompagnie("Compagnie01");
		notification3.setDate("14/11/18");
		notification3.setNomStagiaire(user03.getEmail());
		notification3.setSession("H2017");
		notification3.setUserId(6);
		notification3.setPosteARemplir("Programmeur");


		notification4.setCompagnie("Compagnie01");
		notification4.setDate("14/11/18");
		notification4.setNomStagiaire(user04.getEmail());
		notification4.setSession("H2013");
		notification4.setUserId(11);
		notification4.setPosteARemplir("Programmeur");


		
		notification5.setCompagnie("Compagnie01");
		notification5.setDate("14/11/18");
		notification5.setNomStagiaire(user05.getEmail());
		notification5.setSession("A2018");
		notification5.setUserId(6);
		notification5.setPosteARemplir("Programmeur");


		notificationRepository.save(notification1);
		notificationRepository.save(notification2);
		notificationRepository.save(notification3);
		notificationRepository.save(notification4);
		notificationRepository.save(notification5);
	}
	
	private void initStage() {
		stage = new Stage();
		stage.setSessionStage("test");
		stage.setStagiaire(user01);
		stage.setStage(offre01);
		stageRepository.save(stage);
	}

	public void init() {
		initUser();
		initOffreStage();
		initApplicationStage();
		initNotification();
		initStage();
	}
	
}
