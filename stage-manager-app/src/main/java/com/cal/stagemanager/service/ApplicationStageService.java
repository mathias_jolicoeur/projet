package com.cal.stagemanager.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.cal.stagemanager.domaine.ApplicationStage;
import com.cal.stagemanager.repository.ApplicationStageRepository;

@Service
public class ApplicationStageService {

    @Autowired
    private ApplicationStageRepository applicationStageRepository;
    
    public Page<ApplicationStage> findApplicationValidated(int page, int size){
    	Pageable pageable = PageRequest.of(page, size);
    	return applicationStageRepository.findByapplicationValideTrue(pageable);
    }
    
    public Page<ApplicationStage> findApplicationNotValidated(int page, int size){
    	Pageable pageable = PageRequest.of(page, size);
    	return applicationStageRepository.findByapplicationValideFalse(pageable);
    }
    
    public void setApplicationValidated(int id) {
    	ApplicationStage application = applicationStageRepository.getOne(id);
    	application.setApplicationValide(true);
    	applicationStageRepository.save(application);
    }
    
    public void deleteApplication(int id) {
    	applicationStageRepository.deleteById(id);
    }
    
    public Page<ApplicationStage> findByOfferId(int page, int size, int id) {
    	Pageable pageable = PageRequest.of(page, size);
    	return applicationStageRepository.findByapplicationValideTrueAndOffre_Id(id, pageable);
    }
    public void updateApplication(int id, ApplicationStage app){
        ApplicationStage currApplication = applicationStageRepository.findById(id);
        currApplication.setId(app.getId());
        currApplication.setUser(app.getUser());
        currApplication.setOffre(app.getOffre());
        currApplication.setApplicationValide(app.isApplicationValide());
        currApplication.setStudentTaken(app.isStudentTaken());
        applicationStageRepository.save(currApplication);
    }
    public void accepteStage(int applicationId) {
    	ApplicationStage currApplication = applicationStageRepository.findById(applicationId);
    	currApplication.setStudentAccepted(true);
    	applicationStageRepository.save(currApplication);
    }
}
