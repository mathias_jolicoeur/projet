package com.cal.stagemanager.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.cal.stagemanager.domaine.ApplicationStage;
import com.cal.stagemanager.domaine.ProjectionStage;
import com.cal.stagemanager.domaine.Stage;
import com.cal.stagemanager.repository.ApplicationStageRepository;
import com.cal.stagemanager.repository.OffreStageRepository;
import com.cal.stagemanager.repository.StageRepository;
import com.cal.stagemanager.repository.UserRepository;

@Service
public class StageService {
	
	@Autowired
	private StageRepository stageRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private ApplicationStageRepository appStageRepository;
	
	@Autowired
	private OffreStageRepository offreStageRepository;
	
	public List<Stage> findAll(){
		return stageRepository.findAll();
	}
	
	public Stage findById(int id) {
		return stageRepository.findById(id);
	}
	
	public Page<ProjectionStage> findByQueryStage(int userId, int page, int size){
		Pageable pageable = PageRequest.of(page, size);
		return stageRepository.findByQueryStage(userId, pageable);
	}
	
	public void createStage(int idApp, int idUser) {
		Stage stage = new Stage();
		ApplicationStage app = appStageRepository.findById(idApp);
		stage.setStage(offreStageRepository.findById(app.getOffre().getId()));
		stage.setStagiaire(userRepository.findById(idUser));
		stage.setSessionStage(offreStageRepository.findById(app.getOffre().getId()).getSession());
		stageRepository.save(stage);
	}
	
}
