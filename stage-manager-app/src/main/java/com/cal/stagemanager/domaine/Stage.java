package com.cal.stagemanager.domaine;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor(access=AccessLevel.PUBLIC)
@NoArgsConstructor(access=AccessLevel.PUBLIC)
@Builder 

@Entity
public class Stage {

	@Id
    @GeneratedValue(strategy=GenerationType.TABLE)
    @Column(name="stage_id")
    private int id;
	
	private String sessionStage;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
	private User stagiaire;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "offre_id")
	private OffreStage stage;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSessionStage() {
		return sessionStage;
	}

	public void setSessionStage(String sessionStage) {
		this.sessionStage = sessionStage;
	}

	public User getStagiaire() {
		return stagiaire;
	}

	public void setStagiaire(User stagiaire) {
		this.stagiaire = stagiaire;
	}

	public OffreStage getStage() {
		return stage;
	}

	public void setStage(OffreStage stage) {
		this.stage = stage;
	}

	@Override
	public String toString() {
		return "Stage [id=" + id + ", sessionStage=" + sessionStage + ", stagiaire=" + stagiaire + ", stage=" + stage + "]";
	}
	
}
