package com.cal.stagemanager.domaine;

public class ProjectionStage {

	private int id;
	private String nomCompagnie;
	private String posteARemplir;
	private String description;
	private String sessionStage;
	
	public ProjectionStage(int id,String nomCompagnie, String posteARemplir, String description, String sessionStage) {
		super();
		this.id = id;
		this.nomCompagnie = nomCompagnie;
		this.posteARemplir = posteARemplir;
		this.description = description;
		this.sessionStage = sessionStage;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNomCompagnie() {
		return nomCompagnie;
	}

	public void setNomCompagnie(String nomCompagnie) {
		this.nomCompagnie = nomCompagnie;
	}

	public String getPosteARemplir() {
		return posteARemplir;
	}

	public void setPosteARemplir(String posteARemplir) {
		this.posteARemplir = posteARemplir;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSessionStage() {
		return sessionStage;
	}

	public void setSessionStage(String sessionStage) {
		this.sessionStage = sessionStage;
	}
}
