package com.cal.stagemanager.domaine;

public class ProjectionUserApplication {
	private int id;
	private String nomCompagnie;
	private String posteARemplir;
	private String description;
	private boolean isStudentAccepted;
	
	public ProjectionUserApplication(int id, String nomCompagnie, String posteARemplir, String description, boolean isStudentAccepted) {
		super();
		this.id = id;
		this.nomCompagnie = nomCompagnie;
		this.posteARemplir = posteARemplir;
		this.description = description;
		this.isStudentAccepted = isStudentAccepted;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNomCompagnie() {
		return nomCompagnie;
	}

	public void setNomCompagnie(String nomCompagnie) {
		this.nomCompagnie = nomCompagnie;
	}

	public String getPosteARemplir() {
		return posteARemplir;
	}

	public void setPosteARemplir(String posteARemplir) {
		this.posteARemplir = posteARemplir;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isStudentAccepted() {
		return isStudentAccepted;
	}

	public void setStudentAccepted(boolean isStudentAccepted) {
		this.isStudentAccepted = isStudentAccepted;
	}

}
