package com.cal.stagemanager.domaine;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor(access=AccessLevel.PUBLIC)
@Builder 

@Entity
@PrimaryKeyJoinColumn(name="id")
public class ApplicationStage {

	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	private int id;
	
	@NotNull
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="user_id")
    private User user;

	@NotNull
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="offre_id")
    private OffreStage offre;

	@NotNull
	private boolean applicationValide;
	@NotNull
	private boolean isStudentTaken;
	
	@NotNull
	private boolean isStudentAccepted;
	
	public ApplicationStage() {
		this.applicationValide = false;
		this.isStudentTaken = false;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public OffreStage getOffre() {
		return offre;
	}

	public void setOffre(OffreStage offre) {
		this.offre = offre;
	}

	public boolean isApplicationValide() {
		return applicationValide;
	}

	public void setApplicationValide(boolean applicationValide) {
		this.applicationValide = applicationValide;
	}

	public boolean isStudentTaken() {
		return isStudentTaken;
	}

	public void setStudentTaken(boolean isStudentTaken) {
		this.isStudentTaken = isStudentTaken;
	}

	public boolean isStudentAccepted() {
		return isStudentAccepted;
	}

	public void setStudentAccepted(boolean isStudentAccepted) {
		this.isStudentAccepted = isStudentAccepted;
	}
}
