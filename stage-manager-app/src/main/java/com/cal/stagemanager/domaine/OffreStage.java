package com.cal.stagemanager.domaine;

import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor(access=AccessLevel.PUBLIC)
@NoArgsConstructor(access=AccessLevel.PUBLIC)
@Builder 

@Entity
public class OffreStage{

    @Id
    @GeneratedValue(strategy=GenerationType.TABLE)
    @Column(name="offre_id")
    private int id;
    @NotNull
    private String nomCompagnie;
    @NotNull
    private String posteARemplir;
    @NotNull
    private String description;
    @NotNull
    private double salaire;
    @NotNull
    private boolean isValidated;
    @NotNull
    private String session;
    
    @JsonIgnore
    @OneToMany(mappedBy = "offre")
    private Set<ApplicationStage> applications;

	@OneToMany
	private List<User> usersApplied;
	
	@OneToOne(mappedBy = "stage")
    private Stage stage;
	
	private String dateDepart;

	public int createdById;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNomCompagnie() {
		return nomCompagnie;
	}
	public void setNomCompagnie(String nomCompagnie) {
		this.nomCompagnie = nomCompagnie;
	}
	public String getPosteARemplir() {
		return posteARemplir;
	}
	public void setPosteARemplir(String posteARemplir) {
		this.posteARemplir = posteARemplir;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getSalaire() {
		return salaire;
	}
	public void setSalaire(double salaire) {
		this.salaire = salaire;
	}
	public String getSession() {
		return session;
	}
	public void setSession(String session) {
		this.session = session;
	}
	public boolean isValidated() {
		return isValidated;
	}
	public void setIsValidated(boolean isValidated) {
		this.isValidated = isValidated;
	}
	public Set<ApplicationStage> getApplications() {
		return applications;
	}
	public void setApplications(Set<ApplicationStage> applications) {
		this.applications = applications;
	}
	
	public List<User> getUsersApplied(){
		return this.usersApplied;
	}

	public void setUsersApplied(List<User> users){
		this.usersApplied = users;
	}

	public String getDateDepart(){
		return this.dateDepart;
	}

	public void setDateDepart(String date){
		this.dateDepart = date;
	}
	public int getCreatedById() {
		return createdById;
	}
	public void setCreatedById(int createdById) {
		this.createdById = createdById;
	}
	
	
}