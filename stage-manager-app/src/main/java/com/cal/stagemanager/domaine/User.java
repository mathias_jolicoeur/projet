package com.cal.stagemanager.domaine;

import java.util.Set;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor(access=AccessLevel.PUBLIC)
@NoArgsConstructor(access=AccessLevel.PUBLIC)
@Builder 

@Entity
public class User{

    @Id
    @GeneratedValue(strategy=GenerationType.TABLE)
    @Column(name="user_id")
    private int id;
    @NotNull
    @Column(unique=true)
    private String email;
    @NotNull
    private String type;
    @NotNull
    private String password;
    
    @JsonIgnore
    @OneToMany(mappedBy = "user")
    private Set<ApplicationStage> applications;
    
    @JsonIgnore
    @OneToMany(mappedBy = "stagiaire")
    private Set<Stage> stage;
    
    @Column(length = 1000000)
    private byte[] cv;
    String status;
    @NotNull
    boolean isValidated;
    @Column(length=1000000)
    private byte[] evaluationForm;
    @Column(length=1000000)
    private byte[] hourWorkedForm;
    private Date interviewDate;
    @NotNull
    private boolean isActif = false;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public byte[] getCv() {
		return cv;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Set<ApplicationStage> getApplications() {
		return applications;
	}
	public void setApplications(Set<ApplicationStage> applications) {
		this.applications = applications;
	}
    
	public void setCv(byte[] cv) {
		this.cv = cv;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public boolean isValidated() {
		return isValidated;
	}
	public void setIsValidated(boolean isValidated) {
		this.isValidated = isValidated;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public byte[] getEvaluationForm() {
		return evaluationForm;
	}
	public void setEvaluationForm(byte[] evaluationForm) {
		this.evaluationForm = evaluationForm;
	}
	public byte[] getHourWorkedForm() {
		return hourWorkedForm;
	}
	public void setHourWorkedForm(byte[] hourWorkedForm) {
		this.hourWorkedForm = hourWorkedForm;
	}
	
	public Set<Stage> getStage() {
		return stage;
	}
	public void setStage(Set<Stage> stage) {
		this.stage = stage;
	}
	public void setValidated(boolean isValidated) {
		this.isValidated = isValidated;
	}
	public Date getInterviewDate() {
		return interviewDate;
	}
	public void setInterviewDate(Date interviewDate) {
		this.interviewDate = interviewDate;
	}
	
	public boolean isActif() {
		return isActif;
	}
	public void setActif(boolean isActif) {
		this.isActif = isActif;
	}
	
}