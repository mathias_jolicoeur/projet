package com.cal.stagemanager.domaine;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import lombok.AccessLevel;

@Data
@AllArgsConstructor(access=AccessLevel.PUBLIC)
@NoArgsConstructor(access=AccessLevel.PUBLIC)
@Builder

@Entity

public class Notification{

    @Id
    @GeneratedValue(strategy=GenerationType.TABLE)
	private int id;
	@NotNull
	private int userId;
    @NotNull
    private String compagnie;
    @NotNull
	private String date;
	@NotNull
	private String nomStagiaire;
	@NotNull
	private String session;
	private String posteARemplir;
	
	   
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getCompagnie() {
		return compagnie;
	}
	public void setCompagnie(String compagnie) {
		this.compagnie = compagnie;
    }
    
    public String getDate() {
		return this.date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getNomStagiaire(){
		return this.nomStagiaire;
	}
	public void setNomStagiaire(String nomStagiaire){
		this.nomStagiaire = nomStagiaire;
	}
	public String getSession(){
		return this.session;
	}
	public void setSession(String session){
		this.session = session;
	}
	public String getPosteARemplir(){
		return this.posteARemplir;
	}
	public void setPosteARemplir(String posteARemplir){
		this.posteARemplir = posteARemplir;
	} 

}                                    